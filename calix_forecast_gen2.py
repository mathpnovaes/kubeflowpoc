import dateutil.relativedelta
import glob
import itertools
import os
import pickle
import random
import shap
import sys

import numpy as np
import pandas as pd

from category_encoders import OneHotEncoder
from datetime import *
from timeit import default_timer as timer

# -----------------------------------------------------------------------------
# CALIX MODULES
# -----------------------------------------------------------------------------
from calix_preprocess2 import *
from calix_models_agg2 import *
from calix_models_bup2 import *
from calix_postprocessing import *
from calix_ts_models import TSModel
from calix_model_library import data_setup, create_empty_future
from user_config import USER_PATH

shap.initjs()


# -----------------------------------------------------------------------------
def run_forecast2(cliente_isystems):
    
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "/SETUP/")

    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    dict_setup_fcst_flow,dict_setup_clst_params = run_setup_fcst_flow()
    dict_lowest_level, dict_meta_prod, dict_meta_custom, dict_meta_calend, dict_fato, dict_extra_feat1d_sku, dict_extra_feat1d_custom, dict_extra_feat1d_time, dict_extra_feat3d = run_setup_dict()
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()
    
    # Definindo das variáveis do Fluxo de Forecast
    start_date = dict_setup_fcst_flow['start_training_date']
    number_fcst_cycles = dict_setup_fcst_flow['number_fcst_cycles']
    forecast_horizon = dict_setup_fcst_flow['fcst_horizon']
    sliding_window = dict_setup_fcst_flow['sliding_window']
    cluster_level = dict_setup_fcst_flow['cluster_level']
    model_bup = dict_setup_clst_params['default']['model_bup']
    model_top = dict_setup_fcst_flow['model_top']

    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = start_date+relativedelta(months=number_fcst_cycles)

    print("Início do Treino: " + str(start_date) + " | Término do Treino: " + \
        str(end_date) + " | Horizonte de Previsão: " + str(forecast_horizon) + " meses")

    # Carregando e preprocessando os dados de entrada
    print("\nIniciando a leitura dos dados...")

    df_load = run_load_data2(cliente_isystems)
    print("Iniciando o pré-processamento dos dados...")
    dfinput_prep = run_preprocess2(cliente_isystems, df_load)
    
    # Armazena variáveis caso o preprocessing Lumos tenha sido executado
    try:
        original, data_to_training, validation_data_per_aggregate, \
        validation_keys_per_aggregate = dfinput_prep
        
        # Guarda o pack de variáveis para ser usado mais tarde
        var_pack = dfinput_prep

        # Armazena na variável do fluxo geral o dataframe pré-processado pelo Lumos
        # A função create_empty_future insere no df espaços vazios para alocação das previsões 
        dfinput_prep = create_empty_future(data_to_training)

        # # Insere no df espaços vazios para alocação das previsões
        # dfinput_prep = create_empty_future(dfinput_prep)
    except:
        var_pack = []

    # Contando o número de Séries Temporais que serão executadas no modelo
    df_num_ts = (dfinput_prep['cod_sku'] + '_' + dfinput_prep['cod_cliente']).nunique()
    print('Número total de séries temporais: ' + str(df_num_ts))
    del(df_num_ts)

    t_a = timer()
    current_datetime = datetime.now()
    mean_error = []
    mean_wmape = []
    df_results = pd.DataFrame()
    df_results_tmp = pd.DataFrame()
    df_results_tmp_grp = pd.DataFrame()
    delta = relativedelta(months=sliding_window)
    loop = 1

    # DTW Clustering
    dtw_clustering_on = False
    try:
        dtw_clustering_on = len(dict_setup_fcst_flow['dtw_clustering']) > 0
    except:
        dtw_clustering_on = False

    try: 
        fill_lag_fcst = dict_setup_fcst_flow['fill_lag_fcst']
    except:
        fill_lag_fcst = False

    try:
        ts_models_on = dict_setup_fcst_flow['ts_models']
    except:
        ts_models_on = False

    while start_date < end_date:
        if fill_lag_fcst and loop > 3:
            dfinput_new = fill_lag_fcst(dfinput_prep, df_results)    
            train,tst,xtr,xts,ytr,yts = train_test_split2(cliente_isystems,dfinput_new,start_date,end_date,cluster_level,forecast_horizon,loop,df_results)
        else:
            train,tst,xtr,xts,ytr,yts = train_test_split2(cliente_isystems,dfinput_prep,start_date,end_date,cluster_level,forecast_horizon,loop,df_results)
        
        if dtw_clustering_on:
            if loop in dict_setup_fcst_flow['dtw_clustering']:
                print('Clustering DTW iniciado')
                df_clustering = dfinput_prep[(dfinput_prep[var_when[0]]>=start_date-relativedelta(months=12))&(dfinput_prep[var_when[0]]<start_date)]
                num_skus = df_clustering['cod_sku'].nunique()
                num_clusters = 6 #int(num_skus/30)
                dtw_cluster_dict = get_dtw_clustering(df_clustering, num_clusters)
                print('Clustering DWT concluído')
                del(num_clusters, num_skus, df_clustering)
            try:
                train['DTW_Cluster'] = train['cod_sku'].apply(lambda x: get_sku_dwt_cluster(x, dtw_cluster_dict)).astype('object')
                xtr['DTW_Cluster'] = xtr['cod_sku'].apply(lambda x: get_sku_dwt_cluster(x, dtw_cluster_dict)).astype('object')
                xts['DTW_Cluster'] = xts['cod_sku'].apply(lambda x: get_sku_dwt_cluster(x, dtw_cluster_dict)).astype('object')
            except:
                pass
        print('Preprocessamento Concluído!')

        first_prevision = (xts['timestamp'].max()-relativedelta(months=forecast_horizon-1)).date()
        last_prevision = xts['timestamp'].max().date()

        print('\nPrimeiro mês sendo previsto: ', str(first_prevision))
        print('Último mês sendo previsto:   ', str(last_prevision))
    
        # Calculando previsão agregada (TOP DOWN)
        print("\nPrevisão agregada iniciada...")
        if (model_top == 'Prophet'):
            fcst_agg = run_prophet2(cliente_isystems, train, xts, start_date, forecast_horizon)
        elif (model_top == 'NeuralProphet'):
            fcst_agg = run_neural_prophet(cliente_isystems, train, xts, start_date, forecast_horizon)
        elif (model_top == 'lumos_top'):

            # Chama a função que faz o setup de todas as datas necessárias
            last_valid_month, user_start_validation, user_end_validation, \
                user_end_training, USER_VALIDATION_YEAR = data_setup()

            fcst_agg, mape_agg = run_lumos_top(
                cliente_isystems, data_to_training, original, validation_data_per_aggregate, 
                validation_keys_per_aggregate, start_date, forecast_horizon, user_start_validation, 
                user_end_validation, last_valid_month)
        else:
            print('Modelo Top Down Inexistente!')

        print('Previsão agregada concluída!')

        try:
            dtw_cluster_pipelines = dict_setup_fcst_flow['dtw_cluster_pipelines']
        except:
            dtw_cluster_pipelines = False
#--------------------------------------------------------------------------------------------------------------
#------------------------ INCLUSÃO DA LÓGICA DE PIPELINES POR CLUSTER -----------------------------------------
        if (dict_setup_fcst_flow['cluster_pipelines']==True):
            print("Entrou no Looping da Lógica dos Pipelines")
            df_results_tmp = pd.DataFrame()
            df_results_tmp_grp = pd.DataFrame()
            for x in (dict_setup_fcst_flow['cluster_dim_values']):
                print("Entrou no Looping dos Clusters")
                print("Cluster ",x)
                #Definindo parâmetros gerais do cluster
                try:
                    change_params = dict_setup_clst_params[x]
                    dict_setup_clst_params[x] = copy.deepcopy(dict_setup_clst_params['default'])
                    for key in change_params.keys():
                        dict_setup_clst_params[x][key] = change_params[key]
                except:
                    dict_setup_clst_params[x] = copy.deepcopy(dict_setup_clst_params['default'])
                model_bup = dict_setup_clst_params[x]['model_bup']
                print("Modelo BUP escolhido para o Cluster ",x)
                print(model_bup)

                #Nova segmentação de X e Y para Treino e Teste
                train2 = train[train['cluster_index']==x]
                tst2 = tst[tst['cluster_index']==x]
                
                xtr2, xts2 = train2.drop(['yhat','cluster_index'], axis=1), tst2.drop(['yhat','cluster_index'], axis=1)
                ytr2, yts2 = train2['yhat'].values, tst2['yhat'].values


                # Chamando Função para escolha e execução do modelo BUP
                p, mdl, bup_params, model_name = call_model_bup(x, cliente_isystems, model_bup, xtr2, ytr2, xts2, yts2, var_pack)

                # Pós Processamento e Construção do Dataframe Temporário de Resultados
                temp,temp_grp = post_proc_bup (start_date,p,yts2,tst2,mean_error,mean_wmape)
                
                bup_params = str(bup_params)
                df_results_tmp['bup_params'] = bup_params
                df_results_tmp['model_bup_name'] = model_name

                # Pós Processamento - Cálculo da Reconciliação Padrão
                #temp = post_proc_rec(current_datetime,start_date,temp,df_results_tmp_grp,fcst_agg,bup_params,model_name)

                df_results_tmp_grp = df_results_tmp_grp.append(temp_grp,ignore_index=True)
                df_results_tmp = df_results_tmp.append(temp,ignore_index=True)
            
            try:
                print("Término da Iteração por Cluster")
            
                print("Início da Iteração dos Demais conjuntos de Dados")
                model_bup = dict_setup_clst_params['default']['model_bup']
                
                print("Modelo BUP escolhido para os Demais Conjuntos de Dados ",model_bup)

            
                #Nova segmentação de X e Y para Treino e Teste
                train2 = train[~train['cluster_index'].isin(dict_setup_fcst_flow['cluster_dim_values'])]
                tst2 = tst[~tst['cluster_index'].isin(dict_setup_fcst_flow['cluster_dim_values'])]
                
                xtr2, xts2 = train2.drop(['yhat','cluster_index'], axis=1), tst2.drop(['yhat','cluster_index'], axis=1)
                ytr2, yts2 = train2['yhat'].values, tst2['yhat'].values


                # Chamando Função para escolha e execução do modelo BUP
                p, mdl, bup_params, model_name = call_model_bup(x, cliente_isystems, model_bup, xtr2, ytr2, xts2, yts2, var_pack)

                # Pós Processamento e Construção do Dataframe Temporário de Resultados
                temp,temp_grp = post_proc_bup (start_date,p,yts2,tst2,mean_error,mean_wmape)

                #Registrando os parâmetros utilizados pelo modelo BUP
                bup_params = str(bup_params)
                temp['bup_params'] = bup_params
                temp['model_bup_name'] = model_name

                df_results_tmp_grp = df_results_tmp_grp.append(temp_grp,ignore_index=True)

                # Pós Processamento - Cálculo da Reconciliação Padrão
                #temp = post_proc_rec(current_datetime,start_date,temp,fcst_agg)
                        
                df_results_tmp = df_results_tmp.append(temp,ignore_index=True)
            
                del(temp)
                print("Término da Iteração dos Demais Conjuntos de Dados")
            except Exception:
                pass

        else:
            if ts_models_on:
                print("\nModelo por séries temporais + modelo regressor")
                params = dict_hyper_lgbm['default']
                mdl = TSModel(train, ['cod_sku', 'cod_cliente'], lag_size=12, model_type='light_gbm', features_to_drop=['past_fcst1'])
                mdl.train(params=params)
                
                forecast_data = mdl.forecast(tst)
                bup_params = mdl.params
                relevant_data = tst.columns.tolist()
                selected_data = forecast_data[relevant_data]
                # Pós Processamento e Construção do Dataframe Temporário de Resultados
                df_results_tmp,df_results_tmp_grp = post_proc_bup(start_date,forecast_data['ypred'],forecast_data['yhat'],selected_data,mean_error,mean_wmape)
                
            else:
                print("\nParâmetro de Execução por Cluster não escolhido. Seguindo para Default")
                x='default'
                model_bup = dict_setup_clst_params['default']['model_bup']
                xtr.drop('cluster_index',axis=1,inplace=True)
                xts.drop('cluster_index',axis=1,inplace=True)
            
                # Chamando Função para escolha e execução do modelo BUP
                bup_outputs = call_model_bup(x, cliente_isystems, model_bup, xtr, ytr, xts, yts, var_pack)

                # Separa as variáveis retornadas pelo modelo BUP
                try:
                    fcst_bup, mape_bup = bup_outputs
                except:
                    p, mdl, bup_params, model_name = bup_outputs

                # Pós Processamento e Construção do Dataframe Temporário de Resultados
                df_results_tmp,df_results_tmp_grp = post_proc_bup(start_date,p,yts,tst,mean_error,mean_wmape)
            # Pós Processamento - Cálculo da Reconciliação Padrão
            # df_results_tmp = post_proc_rec(current_datetime,start_date,df_results_tmp,fcst_agg)

        # --------------------------------------------------------------------------------------------------------
        # Necessário enquanto não criamos um código com as possibilidades de reconciliação disponíveis
        if (cliente_isystems == 'AMBEV'):
            
            lumos_previsoes = rec_lumos(fcst_bup, fcst_agg, mape_agg)
        # ------------------------------------------------------------------------------------------------------
        
        #POST PROCESSING - FCST ADJUSTMENT
        #pred_over_class = run_post_error_class_proba(train,tst)
        #train_grouped = run_calc_fcst_bias(train,start_date,4)

        #df_results_tmp = pd.merge(df_results_tmp,pred_over_class,how='left',on=['cod_produto','cod_dpg','ano_mes'],left_index=True)
        #df_results_tmp = pd.merge(df_results_tmp,train_grouped,how='left',on=['cod_produto','cod_dpg'],left_index=True)
        
        #df_results_tmp['fcst_post_adj'] = np.where((df_results_tmp['Label'] == 1)&(df_results_tmp['Score'] >0.9),df_results_tmp['wsnp']*df_results_tmp['var_med_L4M'],df_results_tmp['fcst'])
        
        # --------------------------------------------------------------------------------------------------------
        # Necessário enquanto não generalizamos esta etapa
        if (cliente_isystems == 'AMBEV'): 
        
            lumos_prev_final = lumos_post_pro(lumos_previsoes)

            print("\nConferência ANA")
            print(lumos_prev_final.query("ano_mes == 202104")['revised'].sum())
            print(lumos_prev_final.query("ano_mes == 202105")['revised'].sum())
            print(lumos_prev_final.query("ano_mes == 202106")['revised'].sum())
            print(lumos_prev_final.query("ano_mes == 202107")['revised'].sum())
            print(lumos_prev_final.query("ano_mes == 202108")['revised'].sum())
            print(lumos_prev_final.query("ano_mes == 202109")['revised'].sum())
            print(lumos_prev_final.query("ano_mes >= 202104 & ano_mes <= 202109")['revised'].sum())
        # --------------------------------------------------------------------------------------------------------

        print('\nAjustes de pós-processamento realizados com sucesso!')
        df_results_tmp['run_date'] = current_datetime
        df_results_tmp['as_of'] = start_date
        
        # Pós Processamento - Cálculo da Reconciliação Padrão
        df_results_tmp = post_proc_rec(current_datetime,start_date,df_results_tmp,fcst_agg)

        df_results = df_results.append(df_results_tmp,ignore_index=True)
        df_results['lag'] = ((df_results[var_when[0]]-df_results['as_of'])/np.timedelta64(1, 'M'))        
        df_results['lag'] = np.round(df_results['lag'])
        df_results['eliq_rec'] = df_results['fcst_rec']-df_results['yhat']
        del(df_results_tmp)

        start_date += delta
        loop +=1

    print('----------------------------------------INTERPRETANDO O MODELO-----------------------------------------------------------')
    #explainer = shap.TreeExplainer(mdl)
    #shap_values = explainer.shap_values(xts)

    # summarize the effects of all the features
    #shap.summary_plot(shap_values, xts)
 
    
    print('----------------------------------------ESCOPO DA EXECUÇÃO-----------------------------------------------------------')
    print('Parâmetros de Fluxo do Ciclo de Previsão: \n'+str(dict_setup_fcst_flow))
    print('----------------------------------------')
    print('Parâmetros do Modelo BUP: \n'+str(bup_params))
    print('----------------------------------------')
    print('Variáveis utilizadas para Treino do Modelo BUP: \n'+str(xtr.columns))
    print('-------------------------------------------RESULTADOS----------------------------------------------------------------')
    print('** FCST BUP - RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results[var_target[0]],df_results['fcst']),mape_amb(df_results[var_target[0]],df_results['fcst'])))
    print('** FCST BUP AJST. TOP DOWN - RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results[var_target[0]],df_results['fcst_rec']),mape_amb(df_results[var_target[0]],df_results['fcst_rec'])))
    #print('** FCST BUP AJUST. BIAS - RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results[var_target[0]],df_results['fcfcst_post_adj']),mape_amb(df_results[var_target[0]],df_results['fcst_post_adj'])))

#----------------------------Gráficos de Resíduos -------------------------------------------------    
    #g = sns.JointGrid(data=df_results, x="fcst_rec", y="eliq_rec")
    #g.plot_joint(sns.scatterplot, s=100, alpha=.5)
    #g.plot_marginals(sns.histplot, kde=True)

    #--------------Análise por lag --------------
    
    df_results_lag1 = df_results[df_results['lag']==1]
    df_results_lag2 = df_results[df_results['lag']==2]
    df_results_lag3 = df_results[df_results['lag']==3]
    print('CLIENTE: Fcst BUP Lag1: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1[var_target[0]],df_results_lag1['past_fcst1']),mape_amb(df_results_lag1[var_target[0]],df_results_lag1['past_fcst1'])))
    
    try:
        print('LAG 1: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1[var_target[0]],df_results_lag1['fcst']),mape_amb(df_results_lag1[var_target[0]],df_results_lag1['fcst'])))
        print('LAG 2: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag2[var_target[0]],df_results_lag2['fcst']),mape_amb(df_results_lag2[var_target[0]],df_results_lag2['fcst'])))
        print('LAG 3: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag3[var_target[0]],df_results_lag3['fcst']),mape_amb(df_results_lag3[var_target[0]],df_results_lag3['fcst'])))
    except Exception:
        pass
    
    print('----------------------------Análise Lag 1------------------------------------')
    df_results_lag1_A = df_results_lag1[df_results_lag1['clst_ABC_Class']=='A']
    df_results_lag1_B = df_results_lag1[df_results_lag1['clst_ABC_Class']=='B']
    df_results_lag1_C = df_results_lag1[df_results_lag1['clst_ABC_Class']=='C']
    df_results_lag1_X = df_results_lag1[df_results_lag1['clst_XYZ_Class']=='X']
    df_results_lag1_Y = df_results_lag1[df_results_lag1['clst_XYZ_Class']=='Y']
    df_results_lag1_Z = df_results_lag1[df_results_lag1['clst_XYZ_Class']=='Z']
    
    try:
        print('LAG 1 CURVA A: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1_A[var_target[0]],df_results_lag1_A['fcst']),mape_amb(df_results_lag1_A[var_target[0]],df_results_lag1_A['fcst'])))
        print('LAG 1 CURVA B: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1_B[var_target[0]],df_results_lag1_B['fcst']),mape_amb(df_results_lag1_B[var_target[0]],df_results_lag1_B['fcst'])))
        print('LAG 1 CURVA C: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1_C[var_target[0]],df_results_lag1_C['fcst']),mape_amb(df_results_lag1_C[var_target[0]],df_results_lag1_C['fcst'])))
        print('LAG 1 CURVA X: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1_X[var_target[0]],df_results_lag1_X['fcst']),mape_amb(df_results_lag1_X[var_target[0]],df_results_lag1_X['fcst'])))
        print('LAG 1 CURVA Y: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1_Y[var_target[0]],df_results_lag1_Y['fcst']),mape_amb(df_results_lag1_Y[var_target[0]],df_results_lag1_Y['fcst'])))
        print('LAG 1 CURVA Z: RMSE = %.5f - Mean WMAPE = %.5f' % (rmse(df_results_lag1_Z[var_target[0]],df_results_lag1_Z['fcst']),mape_amb(df_results_lag1_Z[var_target[0]],df_results_lag1_Z['fcst'])))
    except Exception:
        pass    
    
    t_f = timer()
    print("Tempo passado:"+"{:.0f}".format(t_f-t_a)+" s")
      
    #Salvando arquivos de resultados
    path = path_outputs
    day = str('00'+str(current_datetime.day))
    day = day[-2:]
    month = str('00'+str(current_datetime.month))
    month = month[-2:]
    hour = str('00'+str(current_datetime.hour))
    hour = hour[-2:]
    minute = str('00'+str(current_datetime.minute))
    minute = minute[-2:]
    # GERAÇÃO DE CÓDIGO RANDOMICO PARA CRIAÇÃO DE ID DE TESTE
    test_id_random = random.randint(10000, 99999)
    test_id = str(current_datetime.year)+month+day+hour+minute+'_'+str(test_id_random)
    print("\nTEST_ID: ", str(test_id))

    # ARQUIVO FINAL DEVE CONTER O ID DE TESTE + NOME DO ARQUIVO + DATA E HORA DA EXECUÇÃO
    file_name = "id_"+str(test_id)+"_calix_mod_resultados_finais"+".parquet"
    output_file = os.path.join(path,file_name)
    df_results['test_id'] = test_id 
    df_results['cliente_isystems'] = cliente_isystems
    template_resultados = pd.DataFrame(columns=['test_id', 'cliente_isystems','run_date', 'as_of','lag','prod_agg10','prod_agg9','prod_agg8','prod_agg7','prod_agg6','prod_agg5','prod_agg4','prod_agg3','prod_agg2','prod_agg1','cod_sku','custom_agg10','custom_agg9','custom_agg8','custom_agg7','custom_agg6','custom_agg5','custom_agg4','custom_agg3','custom_agg2','custom_agg1','cod_cliente','timestamp','yhat','past_fcst1','past_fcst2','clst_ABC_Class', 'clst_XYZ_Class','clst_lifecycle','fcst','fcst_agg','yhat_tdown','yhat_lower','yhat_upper','fcst_rec','model_bup_name','bup_params'])   
    template_resultados = template_resultados.append(df_results)
    template_resultados.to_parquet(output_file,index=False)
    del(df_results)

    # Agrupando arquivos de Outputs para input no Power BI
    print("\nAgrupamento bases de Resultados Finais para posterior Análise")
    os.chdir(path_outputs)
    extension = 'parquet'
    all_filenames = [i for i in glob.glob('*calix_mod_resultados_finais*.{}'.format(extension))]
    #combine all files in the list
    combined_parquet = pd.concat([pd.read_parquet(f) for f in all_filenames ])
    combined_parquet['year'] = combined_parquet['year'].astype(int)
    combined_parquet['month'] = combined_parquet['month'].astype(int)
    #export to parquet
    combined_parquet.to_parquet( "bi_input_resultados_finais_combined.parquet", index=False)
    
    # Geração de arquivo com LOG dos TESTS_IDS
    teste_combined = combined_parquet.copy()
    del(combined_parquet)
    # Criando index de Séries Temporais
    teste_combined['index_ts'] = teste_combined['cod_sku']+'-'+teste_combined['cod_cliente']
    # Calculando o Erro Absoluto
    teste_combined['eabs_past_fcst1'] = abs(teste_combined['past_fcst1']-teste_combined['yhat'])
    teste_combined['eabs_fcst_bup'] = abs(teste_combined['fcst']-teste_combined['yhat'])
    teste_combined['eabs_fcst_rec'] = abs(teste_combined['fcst_rec']-teste_combined['yhat'])
    # Criando os Erros Quadrados
    teste_combined['se_past_fcst1'] = np.square(teste_combined['eabs_past_fcst1'])
    teste_combined['se_fcst_bup'] = np.square(teste_combined['eabs_fcst_bup'])
    teste_combined['se_fcst_rec'] = np.square(teste_combined['eabs_fcst_rec'])
    
    # Agregando os dados dos testes para cálculo das métricas de avaliação
    teste_combined_agg = teste_combined.groupby('test_id').agg({'eabs_past_fcst1':'sum','eabs_fcst_bup':'sum','eabs_fcst_rec':'sum','se_past_fcst1':'mean','se_fcst_bup':'mean','se_fcst_rec':'mean'})
    teste_combined_agg = teste_combined_agg.reset_index()
    teste_combined_agg = teste_combined_agg.rename({'se_past_fcst1':'mse_past_fcst1','se_fcst_bup':'mse_fcst_bup','se_fcst_rec':'mse_fcst_rec'},axis=1)
    

    teste_combined = teste_combined.groupby('test_id').agg({'run_date':'max',
                                       'as_of': ['min', 'max'],
                                       'cliente_isystems':'max',
                                       'lag':'max',
                                       'timestamp':['min','max'],
                                       'index_ts':'nunique',
                                      'yhat':['sum','max','mean','median']}).reset_index()
    
    teste_combined.columns = teste_combined.columns.droplevel()
    teste_combined.columns = ['test_id','executado_em', 'inicio_treino','fim_treino','cliente_isystems','horizonte_plan','primeiro_mes_plan','ultimo_mes_plan','numero_series','venda_sum','venda_max','venda_mean','venda_median']
    teste_combined = teste_combined.merge(teste_combined_agg,how='left',on='test_id')
    del(teste_combined_agg)    
    
    # Calculando o WMAPE
    teste_combined['wmape_past_fcst1'] = teste_combined['eabs_past_fcst1']/teste_combined['venda_sum']
    teste_combined['wmape_fcst_bup'] = teste_combined['eabs_fcst_bup']/teste_combined['venda_sum']
    teste_combined['wmape_fcst_rec'] = teste_combined['eabs_fcst_rec']/teste_combined['venda_sum']
    
    # Calculando o RMSE
    teste_combined['rmse_past_fcst1'] = np.sqrt(teste_combined['mse_past_fcst1'])
    teste_combined['rmse_fcst_bup'] = np.sqrt(teste_combined['mse_fcst_bup'])
    teste_combined['rmse_fcst_rec'] = np.sqrt(teste_combined['mse_fcst_rec'])
    
    file_name = 'calix_fc_teste_log.csv'
    output_file = os.path.join(path,file_name)
    
    teste_combined.to_csv(output_file,index=False,sep=';',decimal=',')
    print('Arquivo de Log dos Testes atualizado com sucesso!')
    
    return template_resultados,mdl