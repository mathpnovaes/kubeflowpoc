import numpy as np
import pandas as pd

from datetime import datetime, timedelta
from dateutil.relativedelta import *
#from pycaret.classification import *

from calix_preprocess2 import *

# -----------------------------------------------------------------------------
def run_post_error_class_proba2(train, tst):
    
    df_train_class = train.copy()
    df_train_class['max_limit'] = df_train_class['yhat']*1.2
    df_train_class['min_limit'] = df_train_class['yhat']*0.8
    df_train_class['ind_overfcst'] = np.where((df_train_class['past_fcst1'] >= df_train_class['max_limit']),1,0)
    df_train_class['ind_underfcst'] = np.where((df_train_class['past_fcst1'] <= df_train_class['min_limit']),1,0)

    exp_name = setup(data = df_train_class, target ='ind_overfcst',fold_strategy='timeseries',silent=True, ignore_features=['yhat','max_limit','min_limit','ind_underfcst'])
    model = create_model('catboost')
    pred_over_class = predict_model(model, data = tst)
    pred_over_class = pred_over_class[['cod_sku','cod_cliente','timestamp','Label','Score']]
    
    return pred_over_class


# -----------------------------------------------------------------------------
def run_calc_fcst_bias2(train, start_training_date, calc_window):
    
    train = train[train['timestamp']>=start_training_date-relativedelta(months=calc_window)]
    train_grouped = train.groupby(['cod_sku','cod_cliente'])['past_fcst1','yhat'].median().reset_index()
    train_grouped['var_med_L4M'] = train_grouped['yhat']/train_grouped['past_fcst1']
    train_grouped.fillna(0,inplace=True)
    train_grouped = train_grouped[['cod_sku','cod_cliente','var_med_L4M']]
    
    return train_grouped


# -----------------------------------------------------------------------------
def lumos_post_pro(e_data):

    from calix_config import run_setup_hyper
    
    # Definição de variáveis necessárias
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()
    USER_CURRENT_MONTH = dict_hyper_lumos['default']['user_curr_month']
    USER_VALIDATION_YEAR = dict_hyper_lumos['default']['user_validation_year']

    try:
        ano_mes = 202002

        # Seleciona todos os dados posteriores a data de envio da planilha de clusters
        data = e_data.loc[(e_data['ano_mes'] >= ano_mes)].copy()

        # Se a instância estiver marcada como descontinuado == True, então sua previsão é zerada
        data['revised'] = data['revised'].where(data['feat1d_sku2'] == False, 0)
        e_data = data.combine_first(e_data)
    except:
        pass

    # -----------------------------------------------------------------------------------
    # Segunda correção: lógica dos zeros (regra de negócio) p/ o passado - verifica se houve venda p/ o dpg_sku nos últimos 2 meses

    first_prevision = ((USER_VALIDATION_YEAR-2)*100+12) 
    total_months = 25

    ano_mes = first_prevision

    for month_index in range(total_months):
        
        year = ano_mes // 100
        month = ano_mes % 100
        ano_mes_last_2 = (ano_mes-2) if (month > 2)  else (year-1)*100+(month-2)%12
        ano_mes_last_1 = (ano_mes-1) if (month > 1)  else (year-1)*100+12
        ano_mes_next_1 = (ano_mes+1) if (month < 12) else (year+1)*100+(month+1)%12
        
        if (ano_mes <= USER_CURRENT_MONTH):    
            
            print(ano_mes)
            
            # Seleciona o mês seguinte ao ano_mes da iteração
            data = e_data.loc[(e_data['ano_mes'] == ano_mes_next_1)].copy()
        
            # Agrupq por dpg_sku o medido dos últimos 2 meses de dados
            last_two_months = e_data.query('ano_mes == ' + str(ano_mes_last_2) + ' or ano_mes == ' + str(ano_mes_last_1)). \
                                groupby('cod_lowest_level')['yhat'].sum().to_frame()

            # Cria uma lista com os dpg_skus que não tiveram venda nos últimos 2 meses (medido <= 0)
            no_demands = last_two_months.query('yhat <= 0').index.get_level_values('cod_lowest_level').unique().tolist()
            
            # Cria uma lista com os dpg_skus que tiveram venda nos últimos 2 meses (medido > 0)
            with_demands = last_two_months.query('yhat > 0').index.get_level_values('cod_lowest_level').unique().tolist()
            
            # Se não houve venda nos últimos dois meses do dpg_sku, a previsão do mês+1 é zerada
            data['revised'] = data['revised'].where(data.index.get_level_values('cod_lowest_level').isin(with_demands), 0)
        
            e_data = data.combine_first(e_data)
        
            print("-----------------------")
            
        ano_mes = (ano_mes+1) if (month < 12) else (year+1)*100+1
        
    # -----------------------------------------------------------------------------------
    # Terceira correção: lógica dos zeros (regra de negócio) p/ o futuro

    # Seleciona apenas os dados do futuro - previsões
    data = e_data.loc[(e_data['ano_mes'] > USER_CURRENT_MONTH)].copy()

    # Zera as previsões de todos os dpg_skus que foram marcados como sem venda para os últimos 2 meses no loop anterior
    data['revised'] = data['revised'].where(data.index.get_level_values('cod_lowest_level').isin(with_demands), 0)

    e_data = data.combine_first(e_data)

    return e_data


def post_proc_bup (start_date,p,yts,tst,mean_error,mean_wmape):
    p = np.where(p < 0, 0, p)
    # Cálculo de erros e métricas de aderência
    error = rmse(yts, p)
    wmape = mape_amb(yts,p)

    print('\nDate %s - RMSE %.5f - WMAPE %.5f' % (str(start_date), error, wmape))
    mean_error.append(error)
    mean_wmape.append(wmape)
        
    # Construção de DataFrame com resultados
    df_results_tmp,a,b,c,df_results_tmp_grp = pd.DataFrame(),pd.DataFrame(),pd.DataFrame(),pd.DataFrame(),pd.DataFrame()
    a = tst.reset_index(drop=True)
    b = pd.DataFrame(p).reset_index(drop=True)
    b = b.rename({0: 'fcst'}, axis=1)
    df_results_tmp = pd.merge(a,b,left_index=True,right_index=True)
    return df_results_tmp,df_results_tmp_grp


def post_proc_rec (current_datetime,start_date,temp,fcst_agg):
    #print(df_results_tmp.head())
    df_results_tmp_grp = temp.groupby('timestamp')['fcst'].sum().reset_index()
    df_results_tmp_grp = df_results_tmp_grp.rename({'fcst':'fcst_agg'},axis=1)
    df_results_tmp = pd.merge(temp,df_results_tmp_grp[['timestamp','fcst_agg']],on='timestamp',how='left')

    df_results_tmp['run_date'] = current_datetime
    df_results_tmp['as_of'] = start_date    
        
    df_results_tmp = pd.merge(df_results_tmp,fcst_agg[['timestamp','as_of','yhat_tdown','yhat_lower','yhat_upper']],on =['timestamp','as_of'],how='left')
    #df_results_tmp = pd.merge(df_results_tmp,fcst_agg[['timestamp','yhat_tdown','yhat_lower','yhat_upper']],on =['timestamp'],how='left')


    # Reconciliação das previsões top down e bottom up
    df_results_tmp['fcst_rec'] = (df_results_tmp['fcst']/df_results_tmp['fcst_agg'])*df_results_tmp['yhat_tdown']
    df_results_tmp['fcst_rec'] = df_results_tmp['fcst_rec'].fillna(0)
   
    print("\nReconciliação das previsões TOP e BUP efetuada com sucesso!")

    return df_results_tmp