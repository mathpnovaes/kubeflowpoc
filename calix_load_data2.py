import datetime
import dateutil.relativedelta
import itertools
import sys


import numpy as np
import pandas as pd

from datetime import datetime
from datetime import timedelta
from timeit import default_timer as timer


from calix_config import run_system_paths, run_setup_fcst_flow, \
        run_setup_hyper, run_setup_dict, run_set_var_params, \
            run_scope_definition

# -----------------------------------------------------------------------------
# CALIX MODULES
# -----------------------------------------------------------------------------
from user_config import USER_PATH


# -----------------------------------------------------------------------------
def run_load_data2(cliente_isystems):
    print(USER_PATH)
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    print(USER_PATH + cliente_isystems + "/SETUP/")
    sys.path.append(USER_PATH + cliente_isystems + "/SETUP/")

    from calix_config import run_system_paths, run_setup_fcst_flow, \
        run_setup_hyper, run_setup_dict, run_set_var_params, \
            run_scope_definition
    
    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    
    # Leitura da tabela fato e helpers
    df = pd.read_parquet(path_curated + '/tab_fato_appended.parquet')
    df_meta_prod = pd.read_parquet(path_curated + '/tab_meta_prod.parquet')
    df_meta_prod.drop(['as_of', 'cliente_isystems'], 1, inplace=True)
    df_meta_custom = pd.read_parquet(path_curated + '/tab_meta_custom.parquet')
    df_meta_custom.drop(['as_of', 'cliente_isystems'], 1, inplace=True)

    try:
        df_extra_feat1d_sku = pd.read_parquet(path_curated + '/df_extra_feat1d_sku.parquet')
        df_extra_feat1d_sku.drop(['as_of','cliente_isystems'], 1, inplace=True)
        df = pd.merge(df, df_extra_feat1d_sku, on='cod_sku', how='left')
    except Exception:
        pass

    df[['cod_sku']] = df[['cod_sku']].applymap(str)

    # Une os dados auxiliares à tabela fato
    df = pd.merge(df, df_meta_prod, on='cod_sku', how='left')
    df = pd.merge(df, df_meta_custom, on='cod_cliente', how='left')

    # Corrige o dataframe caso merges desnecessários tenham sido feitos
    to_drop = df.columns[df.columns.str.contains("_x")]
    df.drop(to_drop, 1, inplace=True)
    df.columns = df.columns.str.rstrip("_y")

    # Definindo o escopo de execução do modelo
    #out_of_scope = ['Não Atribuído','NO FORECAST']
    df_prot = df.copy()
    #df_prot = df[df['marca']=='BRAHMA'].reset_index(drop=True)
    #df_prot = df[df['feat1d_sku1']=='Mainstream'].reset_index(drop=True)
    #df_prot = df_prot[~df_prot['prod_agg5'].isin(out_of_scope)]
    #df_prot = df_prot[df_prot['feat1d_sku2']!=1.0]
    del(df)

    df_prot['month'] = df_prot['timestamp'].dt.month
    df_prot['year'] = df_prot['timestamp'].dt.year

    df_prot[['month']] = df_prot[['month']].applymap(str)
    df_prot[['year']] = df_prot[['year']].applymap(str)

    return df_prot


# Criação de métricas de avaliação
def rmse(ytrue, ypred):
    return np.sqrt(mean_squared_error(ytrue, ypred))

def mape(actual, pred): 
    actual, pred = np.array(actual), np.array(pred)
    return np.mean(np.abs((actual - pred) / actual)) * 100

def wmape (actual, pred):
    actual, pred = np.array(actual), np.array(pred)
    eabs = abs(actual-pred).sum()
    actual = actual.sum()
    wmape = eabs/actual*100
    return wmape