import copy
import holidays
import pickle
import sys

import numpy as np
import pandas as pd
import pmdarima as pm

from datetime import *
from fbprophet import Prophet
from neuralprophet import NeuralProphet


# -----------------------------------------------------------------------------
# CALIX MODULES
# -----------------------------------------------------------------------------
from calix_preprocess2 import *
from calix_model_library import *
from user_config import USER_PATH


# -----------------------------------------------------------------------------
def run_prophet2(cliente_isystems, df_input_train, xts, start_date, num_periods_fcst):
    
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")
    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    
    fcst = []
    current_datetime = datetime.now()
    df_input_train = run_group_agg_6(df_input_train)
    df_input_train = df_input_train.rename({'timestamp': 'ds', 'yhat': 'y'}, axis=1)
    
    # Definindo os feriados presentes no histórico e futuro
    br_holidays = holidays.Brazil(years=[2015, 2016, 2017, 2018, 2019, 2020])
    br_holidays = pd.DataFrame(br_holidays, index=range(1)).T.reset_index()
    br_holidays.rename(columns={'index': 'ds', br_holidays.columns[1]: 'holiday'}, inplace=True)
    br_holidays['ds'] = pd.to_datetime(br_holidays['ds'])
    #br_holidays['ds'] = br_holidays['ds'].apply(lambda dt: dt.replace(day=1)) # Coloca no formato 'MS' necessário

    if (cliente_isystems == 'FABER'):
        retorno_escolar = pd.DataFrame({'holiday': 'retorno escolar',
                                        'ds': pd.to_datetime(['2015-08-01', '2016-08-01', '2017-08-01',
                                                            '2018-08-01', '2019-08-01', '2020-08-01'])})
        br_holidays = pd.concat((retorno_escolar, br_holidays))
    
    # Parâmetros do Prophet
    m = Prophet(holidays=br_holidays)
    m.add_country_holidays(country_name='BR')
    #m.add_regressor('wsnp')

    m.fit(df_input_train)
    print('Treino Prophet concluído!')
    
    future = m.make_future_dataframe(periods=num_periods_fcst+2, freq='MS')
    #xts_exog = xts.groupby('ano_mes')['wsnp'].sum().reset_index()
    #future = pd.merge(future,xts_exog,how='left',left_on='ds',right_on='ano_mes',left_index=True)
    #future['wsnp'] = future['wsnp'].fillna(0)

    fcst = m.predict(future)
    print('Previsão agregada via Prophet (TOP DOWN) concluída!')
    
    fcst = fcst[['ds','yhat_lower','yhat_upper','yhat']]
    fcst['as_of'] = start_date
    fcst['run_date'] = current_datetime
    fcst['fcst_model_agg'] ='PROPHET'
    m.plot(fcst)
    fcst = fcst.rename({'ds':'timestamp', 'yhat':'yhat_tdown'}, axis=1)
    
    # Salvando modelo treinado
    pkl_full_path = path_outputs+'\Pickle_TDOWN_Prophet_Model.pkl'  

    with open(pkl_full_path, 'wb') as file:
        pickle.dump(m, file)
        
    return fcst


# -----------------------------------------------------------------------------
def run_neural_prophet(cliente_isystems, df_input_train, xts, start_date, num_periods_fcst):
    
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")
    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    
    fcst = []
    current_datetime = datetime.now()
    df_input_train = run_group_agg_6(df_input_train)
    df_input_train = df_input_train.rename({'timestamp': 'ds', 'yhat': 'y'}, axis=1)
    
    # Definindo os feriados presentes no histórico e futuro
    br_holidays = holidays.Brazil(years=[2015, 2016, 2017, 2018, 2019, 2020])
    br_holidays = pd.DataFrame(br_holidays, index=range(1)).T.reset_index()
    br_holidays.rename(columns={'index': 'ds', br_holidays.columns[1]: 'holiday'}, inplace=True)
    br_holidays['ds'] = pd.to_datetime(br_holidays['ds'])
    #br_holidays['ds'] = br_holidays['ds'].apply(lambda dt: dt.replace(day=1)) # Coloca no formato 'MS' necessário

    if (cliente_isystems == 'FABER'):
        retorno_escolar = pd.DataFrame({'holiday': 'retorno escolar',
                                        'ds': pd.to_datetime(['2015-08-01', '2016-08-01', '2017-08-01',
                                                            '2018-08-01', '2019-08-01', '2020-08-01'])})
        br_holidays = pd.concat((retorno_escolar, br_holidays))
    
    # Parâmetros do Neural_Prophet
    m = NeuralProphet(
    growth='linear',
    changepoints=None,
    n_changepoints=5,
    changepoints_range=0.8,
    trend_reg=0,
    trend_reg_threshold=False,
    yearly_seasonality='auto',
    weekly_seasonality='auto',
    daily_seasonality='auto',
    seasonality_mode='additive',
    seasonality_reg=0,
    n_forecasts=12,
    #n_lags=24,
    num_hidden_layers=0,
    d_hidden=None,
    ar_sparsity=None,
    learning_rate=None,
    epochs=None,
    batch_size=None,
    loss_func='Huber',
    train_speed=None,
    normalize='auto',
    impute_missing=True,
    log_level=None,
)
    #m.add_country_holidays(country_name='BR')
    #m.add_regressor('wsnp')

    metrics = m.fit(df_input_train,freq='MS',validate_each_epoch=True, valid_p=0.3)

    print('Treino NeuralProphet concluído!')
    
    future = m.make_future_dataframe(df_input_train, periods=12, n_historic_predictions=True)
    fcst = m.predict(future)
    forecasts_plot = m.plot(fcst)
    print('Previsão agregada via NeuralProphet (TOP DOWN) concluída!')
    
    fcst = fcst[['ds','yhat1']]
    fcst['yhat_lower']=fcst['yhat1']
    fcst['yhat_upper']=fcst['yhat1']
    fcst['as_of'] = start_date
    fcst['run_date'] = current_datetime
    fcst['fcst_model_agg'] ='NEURAL_PROPHET'

    fcst = fcst.rename({'ds':'timestamp', 'yhat1':'yhat_tdown'}, axis=1)
    
    # Salvando modelo treinado
    pkl_full_path = path_outputs+'\Pickle_TDOWN_NeuralProphet_Model.pkl'  

    with open(pkl_full_path, 'wb') as file:
        pickle.dump(m, file)
        
    return fcst

# -----------------------------------------------------------------------------
def run_lumos_top(
    cliente_isystems, data_to_training, original, validation_data_per_aggregate, 
    validation_keys_per_aggregate, start_date, num_periods_fcst, user_start_validation, 
    user_end_validation, last_valid_month):

    # Referenciando local onde fica armazenado o código CONFIG do cliente
    # sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")
    from calix_config import run_system_paths, run_setup_hyper, run_set_var_params

    # # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    # var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()

    # Definindo variáveis necessárias
    dict_hyper_lumos = dict_hyper_lumos['default']
    USER_VALIDATION_YEAR = dict_hyper_lumos['user_validation_year']
    CONFIGURED_AGGREGATES = dict_hyper_lumos['configured_agg']
    DPG_SKU_PARAMS = dict_hyper_lumos['dpg_sku_params']
    DIFF_WEIGHTED_AVERAGE = dict_hyper_lumos['diff_weighted_average']
    USER_CURRENT_MONTH = dict_hyper_lumos['user_curr_month']
    OPER_WEIGHT = dict_hyper_lumos['oper_weight']
    VERSION_CODE = dict_hyper_lumos['version_code']
    GEO = dict_hyper_lumos['geo']

    t_init = ()

    MODELS = {}
    MAPE_MODELS = {}
    DATA_MODELS = {}

    first_prevision = (USER_VALIDATION_YEAR-1)*100+1
    total_months = 24

    ano_mes = first_prevision
    count_adjumod = {}
    count_neunmod = {}
    count_meanmod = {}

    # Cria dicts nos quais os anos são as chaves, que são inicializadas todas como zero
    for month_index in range(total_months):
        count_adjumod[ano_mes] = 0   
        count_neunmod[ano_mes] = 0   
        count_meanmod[ano_mes] = 0        
        year = ano_mes // 100
        month = ano_mes % 100
        ano_mes = (ano_mes+1) if (month < 12) else (year+1)*100+1

    for agg in CONFIGURED_AGGREGATES:
        
        print("-------------------------------------------------------------------------")
        print(agg)
        print("-------------------------------------------------------------------------")
        
        # Cria o meanmod e o adjumod para cada agregado chamando a função .create_model
        MODELS[agg] = create_model(
            agg, data_to_training, validation_data_per_aggregate, validation_keys_per_aggregate, 
            user_start_validation, user_end_validation, DIFF_WEIGHTED_AVERAGE)
        
        # Cria o neunmod chamando a função .calc_neunmod
        calc_neunmod(MODELS[agg], USER_CURRENT_MONTH)

        print("Inicializando previsões ...")
        # Ponto de partida: adjumod - dynamod e secomod são inicializados iguais a ele
        MODELS[agg].copy_prevision('adjumod', 'dynamod')
        MODELS[agg].copy_prevision('dynamod', 'secomod')
        m1 = setup_dynamic_model(
            MODELS[agg], USER_VALIDATION_YEAR, USER_CURRENT_MONTH, 
            OPER_WEIGHT, DPG_SKU_PARAMS, 'adjumod')
        m1_data_val = MODELS[agg].data_val.copy()
        m1_mapes = mape_mes(m1_data_val, MODELS[agg], USER_VALIDATION_YEAR, USER_CURRENT_MONTH)

        # Ponto de partida: neunmod - dynamod e secomod são inicializados iguais a ele
        MODELS[agg].copy_prevision('neunmod', 'dynamod')
        MODELS[agg].copy_prevision('dynamod', 'secomod')
        m2 = setup_dynamic_model(
            MODELS[agg], USER_VALIDATION_YEAR, USER_CURRENT_MONTH, 
            OPER_WEIGHT, DPG_SKU_PARAMS, 'neunmod')    
        m2_data_val = MODELS[agg].data_val.copy()
        m2_mapes = mape_mes(m2_data_val, MODELS[agg], USER_VALIDATION_YEAR, USER_CURRENT_MONTH)

        # Ponto de partida: meanmod - dynamod e secomod são inicializados iguais a ele
        MODELS[agg].copy_prevision('meanmod', 'dynamod')
        MODELS[agg].copy_prevision('dynamod', 'secomod')
        m3 = setup_dynamic_model(
            MODELS[agg], USER_VALIDATION_YEAR, USER_CURRENT_MONTH, 
            OPER_WEIGHT, DPG_SKU_PARAMS, 'meanmod')    
        m3_data_val = MODELS[agg].data_val.copy()
        m3_mapes = mape_mes(m3_data_val, MODELS[agg], USER_VALIDATION_YEAR, USER_CURRENT_MONTH)

        # 'original' é o dataframe original, apenas cortado para o período de treino e já unido aos clusters (antes do setup_training_data)
        new_original = original.groupby([agg,'year','month']).sum()

        ano_mes = first_prevision

        MAPE_MODELS[agg] = copy.deepcopy(m1_mapes)
        DATA_MODELS[agg] = m1_data_val.copy()

        for month_index in range(total_months):

            year = ano_mes // 100
            month = ano_mes % 100
            ano_mes_1 = (ano_mes+1) if (month < 12) else (year+1)*100+(month+1)%12
            ano_mes_2 = (ano_mes+2) if (month < 11) else (year+1)*100+(month+2)%12

            if (ano_mes < USER_CURRENT_MONTH):

                m1 = m1_mapes[ano_mes]
                m2 = m2_mapes[ano_mes]
                m3 = m3_mapes[ano_mes]

            else:

                m1 = m1_mapes[last_valid_month]
                m2 = m2_mapes[last_valid_month]
                m3 = m3_mapes[last_valid_month]

            print('ano_mes', ano_mes)
            
            # Escolhe quais modelos foram melhores para cada mês dentre o adju, neun e mean de acordo com os MAPEs
            if   ((m3 <= m1) and (m3 <= m2)):
                count_meanmod[ano_mes] += 1
                MAPE_MODELS[agg][ano_mes_2] = m3
                DATA_MODELS[agg] = m3_data_val.query('ano_mes == ' + str(ano_mes_2)).combine_first(DATA_MODELS[agg])
            elif ((m2 <= m1) and (m2 <= m3)):
                count_neunmod[ano_mes] += 1
                MAPE_MODELS[agg][ano_mes_2] = m2
                DATA_MODELS[agg] = m2_data_val.query('ano_mes == ' + str(ano_mes_2)).combine_first(DATA_MODELS[agg])
            elif ((m1 <= m2) and (m1 <= m3)):
                count_adjumod[ano_mes] += 1
                MAPE_MODELS[agg][ano_mes_2] = m1
    #             DATA_MODELS[agg] = m1_data_val.query('ano_mes == ' + str(ano_mes_2)).combine_first(DATA_MODELS[agg])
            else:
                print('what', m1, m2, m3)

            ano_mes = (ano_mes+1) if (month < 12) else (year+1)*100+1
        
        # Chama a função .join_with_original
        DATA_MODELS[agg] = join_with_original(new_original, DATA_MODELS[agg])

        # Remove colunas não mais necessárias
        DATA_MODELS[agg].drop(['meanmod_up', 'meanmod_down', 'dynamod', 'up', 'down'], 1, inplace=True)

    print("-------------------------------------------------------------------------")

    # ---------------------------------
    # Entrada forçada

    if (((VERSION_CODE == 'u') or (VERSION_CODE == 'v')) and (GEO != 'OUTROS')):

            # Lê dados de entrada forçada para CERVEJA
            forced_cerv = pd.read_excel(path_curated + "\\Entrada forçada Março 21_mod.xlsx")
            forced_cerv.columns = ['Geografia', 'Embalagem', '202103']

            # Define quais meses terão suas previsões alteradas (USER_CURRENT_MONTH+1 em diante)
            forced_months = [202103]

            # Copia o ajuste agregado para 'tipo_produto', já que usamos a entrada forçada apenas para este agregado
            copia = DATA_MODELS['prod_agg5'].copy()
            copia = copia.reset_index()

            for month in forced_months:

                # Substitui a previsão do secomod para o ano_mes iterado com o volume proveniente da marretada
                # Note que é necessário fazer uma conversão de unidades (enviam o dado em hL mas o previsor roda com L)
                data_cerv = (forced_cerv.query("Geografia == '" + GEO + "' and Embalagem == 'TT'")[str(month)].values)*100
                copia['secomod'] = copia['secomod'].where(((copia['ano_mes'] != month) | (
                    copia['prod_agg5'] != "CERVEJA")),  data_cerv)

            copia = copia.set_index(['prod_agg5', 'year', 'month'])

            # Retorna para o dict principal o dataframe do agregado já alterado
            DATA_MODELS['prod_agg5'] = copia.copy() 

            print("\nAlterações em consequência dos dados de entrada forçada:")
            print(DATA_MODELS['prod_agg5'].query('year == 2021 & month >= 3')['secomod'])

            # Aumentando o nível de confiança do valor fornecido pela MARRETADA
            # Admite-se que o MAPE é zero, ou seja, o valor da marretada é 100% correto
            MAPE_MODELS['prod_agg5'][202103] = 0

    return DATA_MODELS, MAPE_MODELS
























def run_autoarima (cliente_isystems,df_input_train,xts,start_date,num_periods_fcst):

    import sys
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")
    
    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    path_raw,path_curated,path_outputs = run_system_paths()
    var_what,var_where,var_when,var_features,var_target = run_set_var_params()


    fcst = []
    current_datetime = datetime.now()
    df_input_train = run_group_agg_1(df_prep)
    df_input_train = df_input_train[['ano_mes','medido']].set_index('ano_mes')
        
    # AutoArima com Stepwise Params
    model = pm.auto_arima(df_input_train, seasonal=True, m=12)
    
    print('Treino AutoArima Concluído com sucesso!!')
    model.fit(df_input_train)
    
    future_forecast = model.predict(n_periods=num_periods_fcst)
    
    fcst = m.predict(future_forecast)
    print('Previsão AutoArima concluída com sucesso!!')
    fcst = fcst[['ds','yhat_lower','yhat_upper','yhat']]
    fcst['as_of'] = start_date
    fcst['run_date'] = current_datetime
    fcst['fcst_model_agg'] ='AUTOARIMA'
    m.plot(fcst)
    fcst = fcst.rename({'ds':'ano_mes'},axis=1)
    
    return fcst


