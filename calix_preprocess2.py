import sys

import numpy as np
import pandas as pd

from category_encoders import OneHotEncoder
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from sklearn.metrics import mean_squared_error
from timeit import default_timer as timer
from sklearn.preprocessing import StandardScaler
from tslearn.clustering import TimeSeriesKMeans, silhouette_score

# -----------------------------------------------------------------------------
# CALIX MODULES
# -----------------------------------------------------------------------------
from calix_load_data2 import run_load_data2
from calix_model_library import *
from user_config import USER_PATH


# -----------------------------------------------------------------------------
def run_preprocess2(cliente_isystems, df_prot):

    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "/SETUP/")

    from calix_config import run_system_paths, run_setup_fcst_flow, \
        run_setup_hyper, run_setup_dict, run_set_var_params, \
            run_scope_definition
 
    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    dict_lowest_level, dict_meta_prod, dict_meta_custom, dict_meta_calend, \
        dict_fato, dict_extra_feat1d_sku, dict_extra_feat1d_custom, \
            dict_extra_feat1d_time, dict_extra_feat3d = run_setup_dict()
    dict_setup_fcst_flow, dict_setup_clst_params = run_setup_fcst_flow()

    RUN_LUMOS_PREPRO = dict_setup_fcst_flow['run_lumos_prepro']
    COMPLETE_TS = dict_setup_fcst_flow['complete_ts_lags']

    # Zerando valores de venda negativos caso existam
    df_prot[var_target[0]] = df_prot[var_target[0]].apply(lambda x: 0 if x<0 else x)

    # Filtrando os dados conforme escopo do cliente
    df_prot = run_scope_definition(df_prot)

    # Torna opcional a geração dos lags e o preenchimento dos buracos na TS
    if (COMPLETE_TS):
        # df de variáveis numéricas
        int_features = df_prot.select_dtypes('int').columns.to_list()
        float_features = df_prot.select_dtypes('float').columns.to_list()
        num_features = int_features + float_features
        df_num = df_prot[['cod_cliente','cod_sku','timestamp']+num_features]

        # df de variáveis categóricas
        cat_features = df_prot.select_dtypes('object').columns.to_list()
        df_cat = df_prot[cat_features]

        # Criação de df com séries temporais completas (mês a mês)
        start = df_num['timestamp'].min()
        end = df_num['timestamp'].max()+relativedelta(months=+1)
        timestamp_list = pd.date_range(start, end, freq='1M')-pd.offsets.MonthBegin(1)
        timestamp_list_size = len(timestamp_list)
        # variável dummy para forçar séries a terem o tamanho desejado
        dummy_df = pd.DataFrame({'timestamp':timestamp_list, 'cod_sku':'dummy','cod_cliente':'dummy'})
        for feature in num_features:
            dummy_df[feature] = 0
        
        df_num = pd.concat([df_num, dummy_df])
        full_series_df = pd.pivot_table(df_num, values=num_features, index='timestamp', columns=['cod_sku', 'cod_cliente'], aggfunc=np.sum, fill_value=0)
        # extrai cada variável numérica do full_series_df
        df_aux_list = []
        for feature in num_features:
            df_aux = pd.melt(full_series_df[feature], ignore_index=False, value_name=feature).reset_index()
            df_aux.set_index(['cod_sku', 'cod_cliente', 'timestamp'], inplace=True)
            df_aux_list.append(df_aux.copy())

        full_series_df = pd.concat(df_aux_list, axis=1).reset_index()

        full_series_df = full_series_df[full_series_df['cod_sku']!='dummy']
        full_series_df['month'] = full_series_df['timestamp'].dt.month.astype(str)
        full_series_df['year'] = full_series_df['timestamp'].dt.year.astype(str)
        
        # Voltando as infos do df original
        full_series_df = full_series_df.set_index(['cod_sku','cod_cliente'])
        df_full = pd.merge(full_series_df, df_cat.drop(columns=['month', 'year']), on=['cod_sku','cod_cliente'], how='right')

        df_prot = df_full.reset_index()
        df_prot = df_prot.drop(columns=['index'])

        # fill categorical missing data
        cat_features_missing_data = {}
        cat_features = df_prot.select_dtypes('object').columns.to_list()
        for feature in cat_features:
            if df_prot[feature].isna().sum() > 0:
                cat_features_missing_data[feature] = 'OUTRO'
        if len(cat_features_missing_data) > 0:
            df_prot.fillna(cat_features_missing_data, inplace=True)
        df_prot = df_prot.drop_duplicates()
        if not dict_setup_fcst_flow['ts_models']:
            # Criando variável com Lag de Vendas
            lag_list = ['12', '6']
            for L in lag_list:
                df_prot['yhat_lag'+L] = df_prot.groupby(['cod_sku','cod_cliente'])['yhat'].shift(int(L))
                df_prot['yhat_lag'+L].fillna(0,inplace=True)
    
    # Eliminando colunas com excessivos nulos
    df_prot = df_prot[df_prot.columns[df_prot.isnull().mean() < 0.8]]
    print("Limpeza de nulos concluída!")

    # Roda o pré-processamento do Lumos
    if (RUN_LUMOS_PREPRO):
        print("\nIniciando pré-processamentos Lumos...")
        original, data_to_training, validation_data_per_aggregate, \
            validation_keys_per_aggregate = run_lumos_prepro(df_prot)

    try:
        # quando o preprocessing Lumos é executado
        return original, data_to_training, validation_data_per_aggregate, \
        validation_keys_per_aggregate
    except:
        # quando apenas o preprocessing default é executado
        return df_prot


# -----------------------------------------------------------------------------
def gen_time_series2 (df_prep,start_date,fcst_horizon):
    #var_what,var_where,var_when,var_features,var_target = run_set_var_params()
    df_ts_temp = df_prep[var_what+var_where].drop_duplicates(subset=None, keep='first', inplace=False).reset_index(drop=True)
    time_series = pd.date_range(start_date, periods=fcst_horizon, freq="MS")
    time_series = pd.DataFrame(time_series,columns=var_when)
    df_time_series = pd.DataFrame()
    for x in range(0,df_ts_temp.shape[0]):
        df_filtered = df_ts_temp[df_ts_temp.index.isin([x])]
        df_filtered_rep = pd.concat([df_filtered]*len(time_series), ignore_index=True)
        temp = pd.merge(time_series,df_filtered_rep,how='left',left_index=True,right_index=True)
        df_time_series = df_time_series.append(temp)
    print('Geração das Séries Temporais Concluída')
    return df_time_series

# -----------------------------------------------------------------------------
# Criação de função agregadora de nível 6
def run_group_agg_6 (df_input):
    agg6 = ['timestamp']
    df_prot_agg6 = df_input.groupby(agg6[0])['yhat'].sum().reset_index()
    return df_prot_agg6
    
# Nível 2 - Por Estado
def run_group_agg_5 (df_input):
    agg5 = ['timestamp','custom_agg5']
    df_prot_agg5 = df_input.groupby(agg5)['yhat','past_fcst1'].sum().reset_index()
    return df_prot_agg5,agg5
    
def run_group_agg_4 (df_input):
    agg4 = ['timestamp','prod_agg4']
    df_prot_agg4 = df_input.groupby(agg4)['yhat','past_fcst1'].sum().reset_index()
    return df_prot_agg4,agg4
    
def run_group_agg_3 (df_input):
    agg3 = ['timestamp','prod_agg3']
    df_prot_agg3 = df_input.groupby(agg3)['yhat','past_fcst1'].sum().reset_index()
    return df_prot_agg3,agg3
    
def run_group_agg_2 (df_input):
    agg2 = ['timestamp','prod_agg2']
    df_prot_agg2 = df_input.groupby(agg2)['yhat','past_fcst1'].sum().reset_index()
    return df_prot_agg2,agg2
    
def run_group_agg_1 (df_input):
    agg1 = ['timestamp','prod_agg1']
    df_prot_agg1 = df_input.groupby(agg1)['yhat','past_fcst1'].sum().reset_index()
    return df_prot_agg1,agg1


# -----------------------------------------------------------------------------

def fill_lag_fcst(dfinput, df_results):
    import sys
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")

    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    path_raw,path_curated,path_outputs = run_system_paths()
    var_what,var_where,var_when,var_features,var_target = run_set_var_params()
    dict_lowest_level,dict_meta_prod,dict_meta_custom,dict_meta_calend,dict_fato,dict_extra_feat1d_sku,dict_extra_feat1d_custom,dict_extra_feat1d_time,dict_extra_feat3d = run_setup_dict()
    dict_setup_fcst_flow,dict_setup_clst_params = run_setup_fcst_flow()

    pivot = []
    pivot = df_results[[var_what[0],var_where[0],var_when[0],'lag','fcst']].pivot_table(index=['cod_sku','cod_cliente','timestamp'],columns=['lag'],values=['fcst'],aggfunc=np.sum)
    pivot.columns = pivot.columns.get_level_values(1)
    pivot.reset_index(inplace=True)
    pivot.rename({1:'lag1_fcst',
            2:'lag2_fcst',
            3:'lag3_fcst',
            4:'lag4_fcst',
            5:'lag5_fcst',
            6:'lag6_fcst',
            7:'lag7_fcst',
            8:'lag8_fcst',
            9:'lag9_fcst',
            10:'lag10_fcst',
            11:'lag11_fcst',
            12:'lag12_fcst',
            13:'lag13_fcst'},axis=1,inplace=True)
    dfinput = pd.merge(dfinput,pivot,how='left',on=['cod_sku','cod_cliente','timestamp'])#,left_index=True)
    return dfinput

def train_test_split2(cliente_isystems, dfinput, start_date, end_date, cluster_level, fcst_horizon, loop,df_results):    
    
    import sys
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "/SETUP/")

    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    path_raw,path_curated,path_outputs = run_system_paths()
    var_what,var_where,var_when,var_features,var_target = run_set_var_params()
    dict_lowest_level,dict_meta_prod,dict_meta_custom,dict_meta_calend,dict_fato,dict_extra_feat1d_sku,dict_extra_feat1d_custom,dict_extra_feat1d_time,dict_extra_feat3d = run_setup_dict()
    dict_setup_fcst_flow,dict_setup_clst_params = run_setup_fcst_flow()


    print("\n-----------------------------------------")
    print('Loop: ' + str(loop))

    end_fcst_date = start_date+relativedelta(months=fcst_horizon)
    
    #Merging o histórico das execuções dos forecasts com xtr e xts
    # if loop>3:
    #     pivot = []
    #     pivot = df_results[[var_what[0],var_where[0],var_when[0],'lag','fcst']].pivot_table(index=['cod_sku','cod_cliente','timestamp'],columns=['lag'],values=['fcst'],aggfunc=np.sum)
    #     pivot.columns = pivot.columns.get_level_values(1)
    #     pivot.reset_index(inplace=True)
    #     pivot.rename({1:'lag1_fcst',
    #            2:'lag2_fcst',
    #            3:'lag3_fcst',
    #            4:'lag4_fcst',
    #            5:'lag5_fcst',
    #            6:'lag6_fcst',
    #            7:'lag7_fcst',
    #            8:'lag8_fcst',
    #            9:'lag9_fcst',
    #            10:'lag10_fcst',
    #            11:'lag11_fcst',
    #            12:'lag12_fcst',
    #            13:'lag13_fcst'},axis=1,inplace=True)
    #     dfinput = pd.merge(dfinput,pivot,how='left',on=['cod_sku','cod_cliente','timestamp'])#,left_index=True)
    # else:
    #     pass


    train = dfinput[dfinput[var_when[0]] < start_date]
    tst = dfinput[(dfinput[var_when[0]] >= start_date)&(dfinput[var_when[0]] <= end_fcst_date)] 
    
    

    #Clusterização de Séries conforme características
    train_cluster = run_clustering2(cliente_isystems,train,cluster_level,start_date)
    
    train = pd.merge(train,train_cluster,how='left',on=var_what+var_where)
    tst = pd.merge(tst,train_cluster,how='left',on=var_what+var_where)
    
    # Definindo regras de Nulos para cada agrupamento
    train[['clst_ABC_Class']] = train[['clst_ABC_Class']].fillna('C')
    train[['clst_XYZ_Class']] = train[['clst_XYZ_Class']].fillna('Z')
    train[['clst_lifecycle']] = train[['clst_lifecycle']].fillna('NEW')

    tst[['clst_ABC_Class']] = tst[['clst_ABC_Class']].fillna('C')
    tst[['clst_XYZ_Class']] = tst[['clst_XYZ_Class']].fillna('Z')
    tst[['clst_lifecycle']] = tst[['clst_lifecycle']].fillna('NEW')
    
    #----------------------- Incluindo Cluster Index------------------------------------
    clusters_dim = dict_setup_fcst_flow['cluster_dimensions']
    if len(dict_setup_fcst_flow['cluster_dimensions'])>0:
        cluster_index_train = train[clusters_dim[0]]
        cluster_index_tst = tst[clusters_dim[0]]
        for i in clusters_dim[1:]:
            cluster_index_train = cluster_index_train+'-'+train[i].astype(str)
            cluster_index_tst = cluster_index_tst+'-'+tst[i].astype(str)
    train['cluster_index'] = cluster_index_train
    tst['cluster_index'] = cluster_index_tst


    #Definição das variáveis independentes e dependentes
    xtr, xts = train.drop('yhat', axis=1), tst.drop('yhat', axis=1)
    ytr, yts = train['yhat'].values, tst['yhat'].values
    
    print("Split entre Treino e Teste realizado com Sucesso!!")
    return (train,tst,xtr,xts,ytr,yts)


# -----------------------------------------------------------------------------
# Criação de métricas de avaliação
def rmse(ytrue, ypred):
    return np.sqrt(mean_squared_error(ytrue, ypred))

def mape(actual, pred): 
    actual, pred = np.array(actual), np.array(pred)
    return np.mean(np.abs((actual - pred) / actual)) * 100

def mape_amb (actual, pred):
    actual, pred = np.array(actual), np.array(pred)
    eabs = abs(actual-pred).sum()
    actual = actual.sum()
    wmape = eabs/actual*100
    return wmape

def run_ohe (xtr,xts,cols):
    print('-----One-Hot Encoding Iniciado!-----')
    ohe = OneHotEncoder(cols, use_cat_names=True, drop_invariant=True)
    xtr = ohe.fit_transform(xtr)
    xts = ohe.transform(xts)
    print('-----One-Hot Encoding Concluído!-----')
    return xtr,xts

def target_diff(x):
    return x.diff().shift(-1) 


#-------------------------- BINNING / CLUSTERING ------------------------------
def ABC_segmentation(perc):
    '''
    Creates the 3 classes A, B, and C based 
    on quantity percentages (A-60%, B-25%, C-15%)
    '''
    if perc > 0 and perc < 0.6:
        return 'A'
    elif perc >= 0.6 and perc < 0.85:
        return 'B'
    elif perc >= 0.85:
        return 'C'

def XYZ_segmentation(coef):
    '''
    Creates the 3 classes X, Y, and Z based 
    on variability (A-60%, B-25%, C-15%)
    '''
    if coef > 1 and coef < 99999:
        return 'Z'
    elif coef >= 0.6 and coef < 1:
        return 'Y'
    elif coef >= 0:
        return 'X'

def get_dtw_clustering(df, number_clusters):
    scaler = StandardScaler()
    df = pd.pivot_table(df, values='yhat', index='timestamp', columns=['cod_sku'], aggfunc=np.sum, fill_value=0)
    normalized_df = pd.DataFrame(scaler.fit_transform(df))
    normalized_df.columns = df.columns
    normalized_df.index = df.index
    normalized_df = normalized_df.transpose()
    km = TimeSeriesKMeans(n_clusters=number_clusters, metric="dtw", 
                          max_iter=30, n_jobs=-1,
                          tol=1e-5, random_state=42)
    labeled_df = pd.DataFrame({'cod_sku': normalized_df.index, 'labels':km.fit_predict(normalized_df)})
    label_dict = labeled_df.set_index('cod_sku').to_dict()['labels']
    del(df, normalized_df, labeled_df)
    return label_dict

def get_sku_dwt_cluster(sku, dtw_cluster_dict):
    max_cluster = max(dtw_cluster_dict.values())
    cluster = max_cluster + 1
    try:
        cluster = dtw_cluster_dict[sku]
    except:
        cluster = max_cluster + 1
    return cluster

def run_clustering2 (cliente_isystems,df_prep,cluster_level,start_training_date):
    
    import sys
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "/SETUP/")

    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    path_raw,path_curated,path_outputs = run_system_paths()
    var_what,var_where,var_when,var_features,var_target = run_set_var_params()
    dict_lowest_level,dict_meta_prod,dict_meta_custom,dict_meta_calend,dict_fato,dict_extra_feat1d_sku,dict_extra_feat1d_custom,dict_extra_feat1d_time,dict_extra_feat3d = run_setup_dict()


    
    df_prep12LM = df_prep[(df_prep[var_when[0]]>=start_training_date-relativedelta(months=12))&(df_prep[var_when[0]]<start_training_date)]

    #dtw_cluster_dict = get_dtw_clustering(df_prep12LM)

    df_prep12LM['com_venda'] = df_prep12LM[var_target[0]].apply(lambda x: 1 if x>0 else 0)
    df_prep12LM_grouped = df_prep12LM.groupby(cluster_level)['yhat'].describe()
    df_prep12LM_grouped['coef_var'] = df_prep12LM_grouped['std']/df_prep12LM_grouped['mean']
    df_prep12LM_grouped['coef_var'] = df_prep12LM_grouped['coef_var'].fillna(999)
    df_prep12LM_grouped = df_prep12LM_grouped.sort_values(by='mean',ascending=False)
    df_prep12LM_grouped['max_min_dif'] = df_prep12LM_grouped['max']/df_prep12LM_grouped['min']
    
    df_prep12LM_skew = df_prep12LM.groupby(cluster_level)[var_target[0]].skew()
    df_prep12LM_grouped = pd.merge(df_prep12LM_grouped,df_prep12LM_skew,how='left',left_index=True,right_index=True).rename({var_target[0]:'skew'},axis=1)
    
    df_prep12LM_sum = df_prep12LM.groupby(cluster_level)[var_target[0]].sum()
    df_prep12LM_com_venda = df_prep12LM.groupby(cluster_level)['com_venda'].sum()

    df_prep12LM_grouped = pd.merge(df_prep12LM_grouped,df_prep12LM_sum,how='left',left_index=True,right_index=True).rename({var_target[0]:'sum'},axis=1)
    df_prep12LM_grouped = pd.merge(df_prep12LM_grouped,df_prep12LM_com_venda,how='left',left_index=True,right_index=True).rename({var_target[0]:'meses_com_venda'},axis=1)
    
    df_prep12LM_grouped = df_prep12LM_grouped.sort_values(by='sum',ascending=False)
    df_prep12LM_grouped['cum_sum'] = df_prep12LM_grouped['sum'].cumsum()
    df_prep12LM_grouped['TotSum'] = df_prep12LM_grouped['sum'].sum()
    df_prep12LM_grouped['RunPercSum'] = df_prep12LM_grouped['cum_sum']/df_prep12LM_grouped['sum'].sum()
    
    df_prep12LM_grouped['ABC_Class'] = df_prep12LM_grouped['RunPercSum'].apply(ABC_segmentation)
    df_prep12LM_grouped['XYZ_Class'] = df_prep12LM_grouped['coef_var'].apply(XYZ_segmentation)
    df_prep12LM_grouped['Lifecycle'] = df_prep12LM_grouped['com_venda'].apply(lambda x: "NEW" if x<12 else "MATURE")
    df_prep12LM_grouped = df_prep12LM_grouped.rename({'count':'clst_count', 'mean': 'clst_mean', 'std': 'clst_std', 'min': 'clst_min', '25%': 'clst_1st_quartile', '50%':'clst_median','75%':'clst_3rd_quartile', 'max':'clst_max', 'coef_var': 'clst_coef_var', 'max_min_dif':'clst_max_min_dif', 'skew':'clst_skew', 'sum':'clst_sum', 'com_venda':'clst_com_venda','cum_sum':'clst_cum_sum', 'TotSum':'clst_tot_sum', 'RunPercSum':'clst_RunPercSum', 'ABC_Class':'clst_ABC_Class', 'XYZ_Class':'clst_XYZ_Class','Lifecycle':'clst_lifecycle'},axis=1)
    to_drop=['clst_count','clst_mean','clst_std','clst_min','clst_1st_quartile','clst_median','clst_3rd_quartile','clst_max','clst_coef_var','clst_max_min_dif','clst_skew','clst_sum','clst_com_venda','clst_cum_sum','clst_tot_sum','clst_RunPercSum']
    df_prep12LM_grouped =df_prep12LM_grouped.drop(columns=to_drop)
    df_prep12LM_grouped = df_prep12LM_grouped.reset_index()

    #df_prep12LM_grouped['DTW_Cluster'] = df_prep12LM_grouped['cod_sku'].apply(lambda x: dtw_cluster_dict[x])
    #df_prep12LM_grouped['DTW_Cluster'] = df_prep12LM_grouped['DTW_Cluster'].astype('object')
    
    return df_prep12LM_grouped

# -----------------------------------------------------------------------------
def run_lumos_prepro(input_data):

    from calix_config import run_system_paths, run_setup_fcst_flow, \
    run_setup_hyper, run_setup_dict, run_set_var_params, \
        run_scope_definition
    
    # Definição de variáveis necessárias
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()
    CONFIGURED_AGGREGATES = dict_hyper_lumos['default']['configured_agg']
    USER_START_TRAINING = dict_hyper_lumos['default']['user_start_train']
    USER_PREVISIONS = input_data.columns[input_data.columns.str.contains(
            'past_fcst')].to_list()

    # Faz o setup das datas necessárias para a execução
    last_valid_month, user_start_validation, user_end_validation, \
         user_end_training, USER_VALIDATION_YEAR = data_setup()

    # Cria a coluna ano_mes, caso ela não exista
    input_data['ano'] = input_data['timestamp'].dt.strftime("%Y")
    input_data['mes'] = input_data['timestamp'].dt.strftime("%m")
    input_data['ano_mes'] = input_data['ano'] + input_data['mes']
    input_data['ano_mes'] = input_data['ano_mes'].astype('int64')
    input_data.drop(['ano', 'mes'], 1, inplace=True)

    # Transforma year e month em inteiros
    input_data.year = input_data.year.astype("int64")
    input_data.month = input_data.month.astype("int64")

    # Instancia a classe DataFile e passa o dataframe que será pré-processado
    data_file = DataFile(input_data)
    data_file.read()

    print("--------------------------------------------------")
    print("Arquivo original:")
    print("--------------------------------------------------")
    data_file.print_data(data_file.original, CONFIGURED_AGGREGATES)
    print("--------------------------------------------------")

    # É o input_data c/ algumas transformações de tipo de dados e 1 nova coluna
    original = data_file.original

    # A função .setup_training_data() faz algumas limpezas no banco de dados
    data_to_training = data_file.setup_training_data(
        CONFIGURED_AGGREGATES, USER_START_TRAINING, user_end_training)
    
    # Atuação da função .setup_validation_data() da classe DataFile
    validation_data_per_aggregate = {}
    validation_keys_per_aggregate = {}

    start = timer()
    print("Inicia configuração dos dados de validação...")
    for m in CONFIGURED_AGGREGATES:

        validation_data_per_aggregate[m], validation_keys_per_aggregate[m] = \
                    data_file.setup_validation_data(m, USER_PREVISIONS, USER_VALIDATION_YEAR)

    validation_data_per_aggregate['cod_lowest_level'], validation_keys_per_aggregate['cod_lowest_level'] = \
                data_file.setup_validation_data('cod_lowest_level', USER_PREVISIONS, USER_VALIDATION_YEAR)
    
    end = timer()
    print("--> Completo! "+ "{:.2f}".format(end-start) + " s.")
    print("--------------------------------------------------")
    
    return original, data_to_training, validation_data_per_aggregate, validation_keys_per_aggregate
    
