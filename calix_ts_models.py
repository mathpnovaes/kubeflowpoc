
# Standard libraries
import numpy as np
import pandas as pd
import random 

# model libraries
import lightgbm as lgb
from catboost import Pool, CatBoostRegressor

# Sys library
import sys

# ---------------------------------------------------------------------------------

def wmape(ytrue: np.array, ypred: np.array) -> float:
        return 100*np.sum(abs(ypred-ytrue))/np.sum(abs(ytrue))

# ---------------------------------------------------------------------------------

class TSModel():
    def __init__(self, 
                 train_data: pd.DataFrame, 
                 group_ids: pd.DataFrame, 
                 features_to_drop: list=[],
                 lag_size: int=12, 
                 model_type: str='light_gbm'
                 ):
        self.__lag_size = lag_size
        self.__model_type = model_type
        if self.__model_type == 'light_gbm':
            self.__default_params = {
                    'num_boost_round':100,
                    'valid_sets':None,
                    'valid_names':None,
                    'fobj':None,
                    'feval':None,
                    'init_model':None,
                    'boosting': 'gbdt',
                    'categorical_feature':'auto',
                    'early_stopping_rounds':None,
                    'evals_result':None,
                    'learning_rate':0.2,
                    'callbacks':None,
                    'verbosity': -1,
                    'bagging_fraction': 0.8,
                    'bagging_freq':50,
                    'max_depth':100,
                    'num_leaves':64,
                    'metric':'l1'
                    }
        self.__train_data = train_data.copy()
        self.__group_ids = group_ids
        self.__features_to_drop = features_to_drop

        # removing previous lag columns, if exists
        to_drop = self.__train_data.columns[self.__train_data.columns.str.contains('lag')]
        self.__train_data.drop(columns=to_drop, inplace=True)
        self.__train_data.sort_values(group_ids+['timestamp'], inplace=True)

        # configure lag columns
        self._lag_columns = ['lag_'+str(i) for i in range(1, self.__lag_size + 1)]
        for lag in range(1, self.__lag_size + 1):
            self.__train_data['lag_'+str(lag)] = self.__train_data.groupby(self.__group_ids)['yhat'].shift(lag)
        self.__train_data.dropna(inplace=True)
        
        self.__train_data['time_idx'] = 12*self.__train_data['year'].astype(int) + self.__train_data['month'].astype(int)
        self.__origin_idx_train = self.__train_data['time_idx'].min()
        self.__train_data['time_idx'] -= self.__origin_idx_train
        self._max_idx_train = self.__train_data['time_idx'].max()

    @property
    def lag_size(self) -> int: 
        return self.__lag_size

    @property
    def train_data(self) -> pd.DataFrame:
        return self.__train_data

    @property
    def train_data_fitted(self) -> pd.DataFrame:
        return self.__train_data_fitted

    def __preprocess_df(self, df: pd.DataFrame) -> [pd.DataFrame]:
        if self.__model_type == 'light_gbm':
            df = df.reset_index(drop=True).set_index(self.__group_ids).copy()
            df.drop(columns=['time_idx','timestamp','year'], inplace=True)
            df.drop(columns=self.__features_to_drop, inplace=True)
            X = df.drop(columns=['yhat']).copy()
            self._cat_features = X.select_dtypes('object').columns.to_list()
            for feature in self._cat_features:
                X[feature] = X[feature].astype('category')
            y = df['yhat'].copy()
            return X, y
        
    def train(self, params: dict = None):
        if params is None:
            params = self.__default_params
        self.params = params
        if self.__model_type == 'light_gbm':
            self.__X_train, self.__y_train = self.__preprocess_df(self.__train_data)
            LGBM_train_data = lgb.Dataset(self.__X_train, label=self.__y_train.ravel(), 
                                          categorical_feature=self._cat_features, 
                                          free_raw_data=False)
            self.model = lgb.train(self.params, LGBM_train_data)
            self.__train_data_fitted = self.__train_data.copy()
            self.__train_data_fitted['ypred'] = self.model.predict(self.__X_train)
            self.__train_data_fitted['ypred'] = self.__train_data_fitted['ypred'].apply(lambda y: y if y > 0 else 0)

    def forecast(self, X):
        df_forecast = X.copy()
        to_drop = df_forecast.columns[df_forecast.columns.str.contains('lag')]
        df_forecast.drop(columns=to_drop, inplace=True)
        df_forecast['time_idx'] = 12*df_forecast['year'].astype(int) + \
                                     df_forecast['month'].astype(int) - self.__origin_idx_train
        #df_forecast.drop(columns=['timestamp','month','year'], inplace=True)
        forecast_start_time_idx = df_forecast['time_idx'].min()
        forecast_final_time_idx = df_forecast['time_idx'].max()
        appended_forecast = self.__train_data_fitted.copy().set_index(self.__group_ids+['time_idx'])
        df_forecast.set_index(self.__group_ids, inplace=True)
        # initialize lag columns
        for i in range(1, self.__lag_size):
            df_forecast['lag_'+str(i)]=0
        
        # forecasting step-by-step
        for time_idx in range(self._max_idx_train + 1, forecast_final_time_idx + 1):
            forecast_sub_df = df_forecast[df_forecast['time_idx']==time_idx].copy()
            forecast_sub_df = forecast_sub_df.reset_index().set_index(self.__group_ids+['time_idx'])
            for index in forecast_sub_df.index.values.tolist():
                for lag in range(1, self.__lag_size + 1):
                    index_past = list(index)
                    index_past[-1] = index_past[-1] - lag
                    index_past = tuple(index_past)
                    forecast_sub_df.at[index, 'lag_'+str(lag)] = appended_forecast.loc[index_past]['ypred']
            X_step, y_step = self.__preprocess_df(forecast_sub_df.reset_index())
            y_step_pred = self.model.predict(X_step)
            X_step['ypred'] = y_step_pred
            # restore deleted columns
            X_step['yhat'] = y_step
            X_step['time_idx'] = time_idx
            X_step['timestamp'] = forecast_sub_df['timestamp'].min()
            X_step['year'] = X_step['timestamp'].dt.year
            X_step = X_step.reset_index().set_index(self.__group_ids+['time_idx'])
            for feature in self.__features_to_drop:
                X_step[feature] = forecast_sub_df[feature]
            
            appended_forecast = pd.concat([appended_forecast, X_step])
        final_forecast = appended_forecast.copy().reset_index()
        final_forecast = final_forecast[final_forecast['time_idx'] >= forecast_start_time_idx]
        final_forecast['ypred'] = final_forecast['ypred'].apply(lambda x: x if x > 0 else 0)
        return final_forecast
# ---------------------------------------------------------------------------------

class TSModelHyperparamSearch():
    def __init__(self, 
                 train_data: pd.DataFrame, 
                 val_data: pd.DataFrame, 
                 group_ids: list,
                 param_grid: dict, 
                 max_trials: int=100, 
                 lag_size: int=12,
                 features_to_drop: list=[]):
        self.train_data = train_data
        self.val_data = val_data
        self.max_trials = max_trials
        self.param_grid = param_grid
        self.lag_size = lag_size
        self.group_ids = group_ids

    def Optimize(self):
        model_list = []
        lgbm_params = {
                    'num_boost_round':50,
                    'valid_sets':None,
                    'valid_names':None,
                    'fobj':None,
                    'feval':None,
                    'init_model':None,
                    'boosting': 'gbdt',
                    'categorical_feature':'auto',
                    'early_stopping_rounds':None,
                    'evals_result':None,
                    'learning_rate':0.1,
                    'callbacks':None,
                    'verbosity': -1,
                    'bagging_fraction': 0.7,
                    'bagging_freq': 40,
                    'max_depth':100,
                    'num_leaves':2**5,
                    'metric':'l1'
                    }

        num_params = []
        for key in self.param_grid.keys():
            num_params.append(len(self.param_grid[key]))

        total_combinations = np.product(num_params)

        if total_combinations < self.max_trials:
            print(f'Numero total de combinações de hiperparâmetros {total_combinations} é inferior a {self.max_trials} número máximo de tentativas. Usando valor reduzido')
        model_list = []
        val_metrics_list = []
        train_metrics_list = []
        param_list = []
        for i in range(self.max_trials):
            print(i)
            for key in lgbm_params.keys():
                if key in self.param_grid.keys():
                    lgbm_params[key] = random.choice(self.param_grid[key])
            model = TSModel(self.train_data, self.group_ids, lag_size=self.lag_size, model_type='light_gbm')
            model.train(params=lgbm_params)

            train_fitted = model.train_data_fitted
            val_forecast = model.forecast(self.val_data)
            val_metrics_list.append(wmape(val_forecast['yhat'], val_forecast['ypred']))
            train_metrics_list.append(wmape(train_fitted['yhat'], train_fitted['ypred']))
            param_list.append(lgbm_params)
            model_list.append(model)
        
        # chossing champion
        best_model_idx = np.argmin(val_metrics_list)
        self.best_model = model_list[best_model_idx]
        self.best_val_score = val_metrics_list[best_model_idx]
        self.best_train_score = train_metrics_list[best_model_idx]