import numpy as np
import pandas as pd

from timeit import default_timer as timer
from datetime import datetime
from dateutil.relativedelta import relativedelta


# ---------------------------------------------------------------------------------------
class ISModel:

    # -----------------------------------------------------------------------------------
    def __init__(self, data_id):
    # -----------------------------------------------------------------------------------
    
        # data_id é o nome do agregado
        self.data_id = data_id
        
    # -----------------------------------------------------------------------------------
    def setup_training(self, data_origin, DIFF_WEIGHTED_AVERAGE):
    # -----------------------------------------------------------------------------------
        
        start = timer()
        # data_origin é o data_to_training
        
        # Dataframe com agrupação por agg, ano e mês da soma do yhat
        print("Agregando dados por mês para a chave " + str(self.data_id))
        self.data = data_origin.groupby([self.data_id, 'year', 'month'])['yhat'].agg(['sum']).copy()
        self.data.columns = ['yhat']
        
        self.starting_training_year = data_origin['year'].min() #2015
        self.end_training_year = data_origin['year'].max() #2020
        
        print("Separando dados de treino ...")
        self.data_train = self.data.copy()

        # Soma do yhat do ano inteiro
        print("Calculando dados anualizados do período de treino ...")
        self.data_ano = self.data_train.groupby([self.data_id, 'year']).agg({'yhat': ['sum', 'count']})
        self.data_ano.columns = ['yhat', 'meses_dados']
        
        # Média do yhat do mês de todos os anos (média do mês para todos os anos)
        print("Calculando dados mensais do período de treino ...")
        self.data_mes = self.data_train.groupby([self.data_id,'month']).agg({'yhat': ['mean', 'mad']})
        self.data_mes.columns = ['yhat_mean', 'meanmod_up']
        self.data_mes['meanmod_down'] = self.data_mes['meanmod_up']
        
        # Cálculo do índice de sazonalidade
        # Média do mês do agregado dividido pela soma de todas as médias do mês do agregado
        self.data_mes['ind_saz_mean'] = \
                        self.data_mes['yhat_mean']/self.data_mes['yhat_mean'].groupby([self.data_id]).sum()
        
        print("Gerando modelo baseado no histórico ...")
        year_list = []
        weight_list = []
        weight = 1.0
        total_weight = 0
        
        for y in range(self.starting_training_year, self.end_training_year+1):
        
            year_list.append(y)
            
            # Fix - 28/01/01
            # Coloca 2020 com peso zero
            if (y == 2020):
                weight_list.append(0)
            else:            
                total_weight += weight
                weight_list.append(weight)
                weight += DIFF_WEIGHTED_AVERAGE
        
        pesos_frame = pd.DataFrame({'year':year_list, 'peso':weight_list}).set_index('year')
        pesos_frame = self.data_ano.join(pesos_frame)
        pesos_frame["peso"] = pesos_frame['peso']*pesos_frame['meses_dados']
        data_mean_total = (pesos_frame['yhat']*pesos_frame["peso"]/
                       pesos_frame.groupby([self.data_id])["peso"].sum()).to_frame().groupby([self.data_id]).sum()
        data_mean_total.columns = ['yhat_mean']
        self.data_mes['meanmod'] = self.data_mes['ind_saz_mean']*data_mean_total['yhat_mean']
        
        end = timer()
        print("--> Completo! "+ "{:.2f}".format(end-start) + " s.")

    # -----------------------------------------------------------------------------------
    def setup_validation(self, data_origin, validation_keys_list, start_validation_date, end_validation_date):
    # -----------------------------------------------------------------------------------
        self.data_val = data_origin
        self.validation_keys_list = validation_keys_list
        self.start_validation_date = start_validation_date
        self.end_validation_date = end_validation_date
        
    # -----------------------------------------------------------------------------------
    def copy_prevision(self, origin, destin):
    # -----------------------------------------------------------------------------------
        
        self.data_val[destin] = self.data_val[origin]
        
    # -----------------------------------------------------------------------------------
    def update_validation_base(self, validation_base):
    # -----------------------------------------------------------------------------------
        
        self.data_val = validation_base.combine_first(self.data_val)
        
    # -----------------------------------------------------------------------------------
    def update_validation_limits(self, prevision):
    # -----------------------------------------------------------------------------------
        
        self.data_val['up']   = self.data_val[prevision] + self.data_val['meanmod_up']
        self.data_val['down'] = self.data_val[prevision] - self.data_val['meanmod_down']
        self.data_val['down'] = self.data_val['down'].where((self.data_val['down'] > 0), 0)

    # -----------------------------------------------------------------------------------
    def setup_mean_model(self):
    # -----------------------------------------------------------------------------------
    # Remove colunas desnecessárias
    # Une o df data_mes (que foi criado em cima do data_to_training) com o data_val de cada agregado
    # Apesar do data_mes vir dos dados de treino seus dados não estão organizados por ano, mas sim por mês
    # Por isso após o combine_first o dataframe resultante continua possuindo apenas os anos dos dados de validação (2020,2021,2022)
    # O data_val resultante sai com yhat, meanmod, meanmod_up, meanmod_down, ano, mes, ano_mes, previsões cliente
    
        start = timer()
        
        print("Montando modelo da média")
        
        # Inclui a previsão média na tabela de ano-mês
        self.data_val = self.data_val.combine_first(self.data_mes)
        self.data_val = self.data_val.reorder_levels([self.data_id,'year','month']).sort_index()
        
        # Retira algumas colunas não úteis depois do merge (retorne se quiser fazer algum debug)
        self.data_val = self.data_val.drop(columns=['yhat_mean', 'ind_saz_mean'])
        self.data_val = self.data_val.fillna(0)
        
        end = timer()
        print("--> Completo! "+ "{:.2f}".format(end-start) + " s.")
        
    # -----------------------------------------------------------------------------------
    def setup_adjusted_model(self):
    # -----------------------------------------------------------------------------------

        start = timer()
        
        print("Calculando modelo ajustado pela tendência anual ...")
        growth_list = []

        for key in self.validation_keys_list:
            last_value = -1
            change = 0
            total_changes = 0
            weight_total = 0
            for y in range(self.starting_training_year, self.end_training_year+1):
                pct_change = -1
                try:         
                    if (self.data_ano.loc[(key,y)]['meses_dados'] == 12) :
                        curr_value = self.data_ano.loc[(key,y)]['yhat'] 
                        curr_year = y
                        if (last_value > 0):
                            c = (curr_value - last_value)/last_value/(curr_year-last_year)
                            if ((c > -1) and (c < 1)):
                                change += c*(y-self.starting_training_year+1)
                                weight_total += (y-self.starting_training_year+1)
                                total_changes += 1
                        if (last_value != 0):
                            last_value = curr_value
                            last_year = curr_year
                except KeyError:
        #             print("X-- not found year change for ["+str(key)+","+str(y)+"]: KeyError")
                    pass
                except TypeError:
        #             print("X-- not found year change for ["+str(key)+","+str(y)+"]: TypeError")
                    pass
            if ((last_value < 0) or (weight_total == 0) or (total_changes < 2)):
                change = 0
            else:
                change = change / weight_total 
            growth_list.append(change)

        df = pd.DataFrame({'growth':growth_list, self.data_id:self.validation_keys_list}).set_index([self.data_id])
        self.data_val['adjumod'] = self.data_val['meanmod']*(df['growth'] + 1)

        end = timer()
        print("--> Completo! "+ "{:.2f}".format(end-start) + " s.")


# ---------------------------------------------------------------------------------------
def create_model(
    column, data_to_training, validation_data_per_aggregate, validation_keys_per_aggregate, 
    user_start_validation, user_end_validation, DIFF_WEIGHTED_AVERAGE):
    
    # 'column' é o agregado
    # Instancia a classe ISModel
    model = ISModel(column)
    
    # Chama a função .setup_training da classe ISModel passando os dados de treino (gerados pela classe DataFile)
    # Gera o meanmod
    model.setup_training(data_to_training, DIFF_WEIGHTED_AVERAGE)
    
    # Chama a função .setup_validation da classe ISModel passando os dados de validação (gerados pela classe DataFile)
    # Só pega parâmetros, não atua em nada no dataframe de validação de cada agregado
    model.setup_validation(validation_data_per_aggregate[column], validation_keys_per_aggregate[column], \
                           user_start_validation, user_end_validation)
    
    # Chama a função .setup_mean_model da classe ISModel (ela só reorganiza o df resultante do setup_training)
    model.setup_mean_model()
    
    # Inicializa o adjumod igualando-o ao meanmod
    model.data_val['adjumod'] = model.data_val['meanmod']
    
    # Chama a função .setup_adjusted_model da classe ISModel
    # Cria o adjumod (atualiza a coluna que foi inicializada como sendo igual ao meanmod)
    model.setup_adjusted_model()
    
    return model


# ---------------------------------------------------------------------------------------
def calc_neunmod(model, USER_CURRENT_MONTH):
    
    # model.data_train são os yhats de cada agregado indexado por chave, ano e mes
    
    # Calcula a mudança percentual de um mês para outro para cada chave de cada agregado
    multipliers = model.data_train.groupby([model.data_id])['yhat'].pct_change(). \
        replace([np.inf, -np.inf], np.nan).dropna().groupby([model.data_id,'month']).mean().to_frame()
    multipliers.columns = ['multiplier']

    # Não permite que existam multiplicadores maiores que 1
    multipliers['multiplier'] = multipliers['multiplier'].where(multipliers['multiplier'] < 1, 1)
    
    # model.data_val é o df de saída da função create_model (mais especificamente da setup_adjusted_model)
    data = model.data_val
    data = data.combine_first(multipliers).reorder_levels([model.data_id,'year','month']).sort_index()
    
    # Criação de novas colunas com valores deslocados
    data['yhat_shift'] = data.groupby([model.data_id])['yhat'].shift(2)
    data['multiplier_shift'] = data.groupby([model.data_id])['multiplier'].shift(1).fillna(0)
    
    # Calcula o final_multiplier o não permite que ele seja maior que 2 para cada mês de cada ano de cada chave de cada agg
    data['final_multiplier'] = (1 + data['multiplier']) * (1 + data['multiplier_shift'])
    data['final_multiplier'] = data['final_multiplier'].where(data['final_multiplier'] < 2, 2)
    
    # Cria o neunmod
    data['neunmod'] = data['yhat_shift'] * data['final_multiplier']
    
    # Não permite que existam instâncias negativas, preenche com o adjumod
    data['neunmod'] = data['neunmod'].where(data['neunmod'] > 0, data['adjumod'])
    
    # Preenche o futuro com o adjumod
    data['neunmod'] = data['neunmod'].where(data['ano_mes'] <= (USER_CURRENT_MONTH+1), data['adjumod'])
    
    # -------** WARNING **-------
    data.drop(['multiplier', 'multiplier_shift', 'yhat_shift', 'final_multiplier'], 1, inplace=True)
    
    model.data_val = data


# ---------------------------------------------------------------------------------------
def setup_dynamic_model(
    model, USER_VALIDATION_YEAR, USER_CURRENT_MONTH, OPER_WEIGHT, params=[], 
    algorithm='adjumod'):
    
    print("Limitando grupo de ajustes ...")
    
    # Chama a função .setup_clean_validation_base
    clean_base = setup_clean_validation_base(model, algorithm, USER_VALIDATION_YEAR)
    
    print("Executa a dinâmica de correção...")
    
    # Chama a função .treina_modelo que aplica a lógica do PID e gera o dynamod e o secomod
    mape, new_base = treina_modelo(model, clean_base, params, 
                                   USER_VALIDATION_YEAR, USER_CURRENT_MONTH, 
                                   OPER_WEIGHT)
    
    print("Atualização base de validação ...")
    
    # Chama a função .update_validation_base da classe ISModel
    model.update_validation_base(new_base)
    
    # Chama a função .update_validation_limits da classe ISModel
    model.update_validation_limits('dynamod')
    
    return mape


# ---------------------------------------------------------------------------------------
def setup_clean_validation_base(model, algorithm, USER_VALIDATION_YEAR):
# Retira da base chaves de agregados que não têm valores de yhat ou do algoritmo para o ano corrente (2021)

    # model.data_val tem neunmod, adjumod e meanmod em suas colunas, além das cópias para o seco e o dyna
    
    # Agrupa os dados do agregado pelo ano
    validation_summary = model.data_val.groupby([model.data_id,'year']).sum()
    
    # Encontra chaves dos agregados que estão zeradas para 2021
    zero_to_zero = validation_summary.query('(yhat == 0) and ('+algorithm+' == 0) and (year == '+str(USER_VALIDATION_YEAR)+')')
       
    prevision_zero_list = zero_to_zero.index.get_level_values(model.data_id).unique().tolist()
    adjustable_keys_list = list(set(model.validation_keys_list)-set(prevision_zero_list))
    
    # Remove das base as chaves dos agregados zeradas para 2021
    clean_validation_base = model.data_val.loc[adjustable_keys_list].copy()

    print(">>> Total de registros na base limpa: " + str(len(clean_validation_base)))
    print(">>> Total original combinations: " + str(len(model.validation_keys_list)))
    print(">>> Combinations that prevision is complete zero: " + str(len(prevision_zero_list)))
    print(">>> Combinations that matters: " + str(len(adjustable_keys_list)))

    return clean_validation_base


# ---------------------------------------------------------------------------------------
def treina_modelo(
    model, data_ano_mes, params, USER_VALIDATION_YEAR,USER_CURRENT_MONTH, 
    OPER_WEIGHT):
    
    # model é o objeto criado pela classe ISModel
    # data_ano_mes é o que o .setup_clean_validation_base retorna
    
    t_a = timer()
        
    KP_T_2 = params[0]
    KI_T_2 = params[1]
    KD_T_2 = params[2]

    # ---------------------------------------------------------------------
    # Inicializa colunas que serão necessárias
    # ---------------------------------------------------------------------
    
    original_columns = data_ano_mes.columns
    
    # Lembrando que o dynamod é inicializado como uma cópia do mean, adju ou neun
    data_ano_mes['error'] = data_ano_mes['yhat'] - data_ano_mes['dynamod']
    data_ano_mes['error_1'] = 0
    data_ano_mes['error_2'] = 0
    
    data_ano_mes['int_erro'] = 0
    data_ano_mes['int_erro_1'] = 0
    data_ano_mes['int_erro_2'] = 0
    
    data_ano_mes['der_error'] = 0
    data_ano_mes['der_error_1'] = 0
    data_ano_mes['der_error_2'] = 0
    
    data_ano_mes['correcao'] = 0
    data_ano_mes['correcao_2'] = 0
    
    # ---------------------------------------------------------------------
    # Lógica dos zeros
    # ---------------------------------------------------------------------
    
    def calc_zeros(x):
        if (x['zeros_last'] == 0):
            return 0
        return x['zeros_last']+x['zeros_last_1']
    
    # Específico dos dados da AmBev
    ZERO_LIMIT = 100
    ZEROS_TO_SET_ZERO = 2 # regra de negócio??
    if (model.data_id == 'prod_agg4'):
        ZERO_LIMIT = 1000
    
    # repetir essa operação até encontrar o número de zeros desejado (assim só encontramos >= 2 zeros seguidos)
    data_ano_mes['zeros'] = 0
    
    # Marca meses com yhat < 100
    data_ano_mes['zeros'] = data_ano_mes['zeros'].where(data_ano_mes['yhat'] > ZERO_LIMIT, 1)
    
    data_ano_mes['zeros_last'] = data_ano_mes.groupby([model.data_id])['zeros'].shift().fillna(0)
    data_ano_mes['zeros_last_1'] = data_ano_mes.groupby([model.data_id])['zeros_last'].shift().fillna(0)
    data_ano_mes['zeros_1'] = data_ano_mes.apply(lambda x: calc_zeros(x), axis=1) 
    data_ano_mes['zeros_2'] = data_ano_mes.groupby([model.data_id])['zeros_1'].shift()

    t_b = timer()

    # ---------------------------------------------------------------------
    # EXECUTA O MODELO
    # ---------------------------------------------------------------------
    
    first_prevision = (USER_VALIDATION_YEAR-1)*100+1
    total_months = 24
    
    ano_mes = first_prevision
    
    print("first_prevision", first_prevision)
    
    for m in range(total_months):
            
#         t_c = timer()
        y = ano_mes // 100
        m = ano_mes % 100
        ano_mes_1 = (ano_mes+1) if (m < 12) else (y+1)*100+(m+1)%12
        ano_mes_2 = (ano_mes+2) if (m < 11) else (y+1)*100+(m+2)%12
        ano_mes_3 = (ano_mes+3) if (m < 10) else (y+1)*100+(m+3)%12

        print(ano_mes)

        if (ano_mes < USER_CURRENT_MONTH):

            tt1 = timer()
            
            # Seleciona apenas as instâncias do ano_mes para todas as chaves de cada agregado
            today = data_ano_mes.loc[(data_ano_mes['ano_mes'] == ano_mes)].copy()
            
            # Calcula o erro, a derivada e a integral do erro para o ano_mes da iteração
            today['error'] = today['yhat'] - today['dynamod']
            today['der_error'] =  today['error'] - today['error_1']
            today['int_erro'] = today['int_erro_1'] + today['error']
            data_ano_mes = today.combine_first(data_ano_mes)
            
            tt2 = timer()

            # Seleciona o ano_mes da iteração mais os próximos dois meses
            data = data_ano_mes.loc[(data_ano_mes['ano_mes'] >= ano_mes) & (data_ano_mes['ano_mes'] <= ano_mes_2)].copy()

            data_T_1 = data.groupby([model.data_id]).shift(periods=1, fill_value=0)
            data_T_2 = data.groupby([model.data_id]).shift(periods=2, fill_value=0)
            data['yhat_1'] = data_T_1['yhat']
            data['yhat_2'] = data_T_2['yhat']

            data['error_1'] = data_T_1['error']
            data['error_2'] = data_T_2['error']

            data['der_error_1'] = data_T_1['der_error']
            data['der_error_2'] = data_T_2['der_error']

            data['int_erro_1'] = data_T_1['int_erro']
            data['int_erro_2'] = data_T_2['int_erro']

            data['oper_1'] = data_T_1['past_fcst1']

            data_ano_mes = data.combine_first(data_ano_mes)

            tt3 = timer()

            # Seleciona o ano_mes_2
            DT2 = data_ano_mes.loc[(data_ano_mes['ano_mes'] == ano_mes_2)].copy()
            
            # Aplica o OPER apenas para o modelo dpg_sku - não é utilizado nos modelos dos agregados
            # PID do erro e criação do dynamod
            if (model.data_id == 'cod_lowest_level'):
                DT2['dynamod'] = (DT2['dynamod'] + \
                                 DT2['error_2']     * KP_T_2 + \
                                 DT2['int_erro_2']  * KI_T_2 + \
                                 DT2['der_error_2'] * KD_T_2) * (1-OPER_WEIGHT) + \
                                 DT2['oper_1'] * OPER_WEIGHT
#             else:
#                 DT2['dynamod'] = DT2['dynamod'] * (1-OPER_WEIGHT/2) + DT2['oper_1'] * OPER_WEIGHT/2
            
            # Esta parte é aplicada tanto para os modelos dos agregados quanto para o modelo dpg_sku
            # Criação do secomod
            DT2['dynamod'] = DT2['dynamod'].where((DT2['dynamod'] < 2*DT2['yhat_2']), 2*DT2['yhat_2'])
            DT2['dynamod'] = DT2['dynamod'].where((DT2['dynamod'] > DT2['yhat_2']/2), DT2['yhat_2']/2)
            DT2['dynamod'] = DT2['dynamod'].where((DT2['dynamod'] > 0) & (DT2['zeros_2'] < ZEROS_TO_SET_ZERO), 0)
            DT2['secomod'] = DT2['dynamod']
            data_ano_mes = DT2.combine_first(data_ano_mes)

            tt4 = timer()            
            
        ano_mes = (ano_mes+1) if (m < 12) else (y+1)*100+1
                
    t_f = timer()
    
    # ---------------------------------------------------------------------
    # Cálculo do mape do ano corrente (USER_VALIDATION_YEAR) com dados fechados
    # ---------------------------------------------------------------------

    vd = data_ano_mes.query("year == "+str(USER_VALIDATION_YEAR)+" & ano_mes < " + str(USER_CURRENT_MONTH))
    total_produced = vd['yhat'].sum()
    mape = (abs(vd['yhat']-vd['secomod']).sum()*100/total_produced)
    
    print("--> tempo de execução: " + "{:.0f}".format((t_f-t_a)/60) + " min")
    print("total_produced = "+"{:.2f}".format(total_produced/100)+ " hL")
    print("mape   = "+"{:.2f}".format(mape)+ " %")
    
    data_ano_mes = data_ano_mes[original_columns]

    return mape, data_ano_mes


# ---------------------------------------------------------------------------------------
def mape_mes(data, model, USER_VALIDATION_YEAR, USER_CURRENT_MONTH):
    
    mape_agg_mes = {}

    first_prevision = (USER_VALIDATION_YEAR-1)*100+1
    total_months = 24

    ano_mes = first_prevision

    sum_abs = []
    sum_tot = []

    for m in range(total_months):

        y = ano_mes // 100
        m = ano_mes % 100

        if (ano_mes < USER_CURRENT_MONTH):
            
            today = data.loc[(data['ano_mes'] == ano_mes)].copy()

            sum_tot.append(today['yhat'].sum())
            sum_abs.append(abs(today['yhat']-today['secomod']).sum())
            if (len(sum_tot) > 6):
                sum_tot = sum_tot[1:]
                sum_abs = sum_abs[1:]
            mape_agg_mes[ano_mes] = sum(sum_abs)*100/sum(sum_tot)

        ano_mes = (ano_mes+1) if (m < 12) else (y+1)*100+1
    
    return mape_agg_mes


# ---------------------------------------------------------------------------------------
def join_with_original(original, data):
    
    # original é o dataframe original, apenas cortado para o período de treinamento
    # data é o resultado da votação do melhor modelo dentre o adju, o mean e o neun
    
    print('data size:', len(data))

    print("Retorna a previsão para a base principal ...")
    final_data = original.combine_first(data).copy()
    final_data = final_data.fillna(0)

    print("Atualizando ano-mes ...")
    final_data['ano_mes'] = final_data.index.get_level_values('year')*100 + final_data.index.get_level_values('month')
    final_data['am'] = final_data.index.get_level_values('year') + final_data.index.get_level_values('month')/13
    
    return final_data


# ---------------------------------------------------------------------------------------
def rec_lumos(dpg_sku_data, DATA_MODELS, MAPE_MODELS):

    from calix_config import run_setup_hyper
    
    # Definição de variáveis necessárias
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()
    CONFIGURED_AGGREGATES = dict_hyper_lumos['default']['configured_agg']
    USER_CURRENT_MONTH = dict_hyper_lumos['default']['user_curr_month']
    USER_VALIDATION_YEAR = dict_hyper_lumos['default']['user_validation_year']
    GEO = dict_hyper_lumos['default']['geo']

    # Ponto de partida para gerar o dataframe final com as previsões (parte-se do arquivo bottom up)
    e_data = dpg_sku_data.copy()

    for m in CONFIGURED_AGGREGATES:
        
        print("-------------------------------------------------------------------------")
        print(m)
        
        # Chama a função calculate_change e armazena o dataframe criado por ela em um dict
        change_dic = calculate_change(m, e_data, DATA_MODELS[m], USER_VALIDATION_YEAR)
        
        # Unifica o dataframe com os erros e o dataframe com as previsões BUP
        e_data = e_data.join(change_dic, on=[m,'year','month']).fillna(0)

    print("-------------------------------------------------------------------------")

    # Cria a coluna 'revised', que vai abrigar as previsões finais - a inicializa com os dados do secomod
    e_data['revised'] = e_data['secomod']

    # Define o ponto de partida como sendo janeiro do ano anterior ao de validação 
    first_prevision = (USER_VALIDATION_YEAR-1)*100+1
    total_months = 24

    ano_mes = first_prevision

    for month_index in range(total_months):

        total = 0
        peso_total = 0

        year = ano_mes // 100
        month = ano_mes % 100
        
        # ----------------------------------------------------------------------------
        # Efetiva aplicação da reconciliação para os meses de validação do passado
        # ----------------------------------------------------------------------------
        if (ano_mes <= USER_CURRENT_MONTH): 

            print(ano_mes)   
            
            # Seleciona os dados do ano_mes iterado
            data = e_data.loc[(e_data['ano_mes'] == ano_mes)].copy()

            for agg in CONFIGURED_AGGREGATES:
                
                # Calcula o "acerto" da previsão TDW para o ano_mes e este é denominado como um peso
                peso = (100-MAPE_MODELS[agg][ano_mes])
                
                # Se houve algum acerto, o peso é ponderado com o erro relativo anteriormente calculado
                # Ou seja, o acerto do TDW é ponderado com o erro relativo entre TDW e BUP
                if (peso > 0):
                    total += (1+data[agg+'_mult'])*peso # total é um dataframe
                    peso_total +=peso                   # peso_total é um número

            if (GEO == 'OUTROS'): # necessário para evitar erro de divisão por zero
                if (peso_total > 0):

                    multiplier = total / peso_total
                    print('>   peso_total =', peso_total)

                    data['revised'] = data['secomod'] * multiplier
                    data['revised'] =\
                        data['revised'].where(data['revised'] > 0, 0)
                    e_data = data.combine_first(e_data)
            else:
                # Dataframe contendo a razão entre total e peso_total para todos os meses de validação para todos os dpg_skus
                multiplier = total / peso_total
                print('>   peso_total =', peso_total)
                
                # Após iterar em todos os agg, se houve acerto então o revised é atualizado
                if (peso_total > 0):
                    data['revised'] = data['secomod'] * multiplier
                    data['revised'] =\
                        data['revised'].where(data['revised'] > 0, 0)
                    e_data = data.combine_first(e_data)

            print("-----------------------")

        ano_mes = (ano_mes+1) if (month < 12) else (year+1)*100+1

    year = USER_CURRENT_MONTH // 100
    month = USER_CURRENT_MONTH % 100

    # Define o primeiro mês sendo previsto
    ano_mes = (USER_CURRENT_MONTH+1) if (month < 12) else (year+1)*100+1

    # Seleciona os meses do futuro
    data = e_data.loc[(e_data['ano_mes'] > USER_CURRENT_MONTH)].copy()

    # ----------------------------------------------------------------------------
    # Efetiva aplicação da reconciliação para o futuro
    # ----------------------------------------------------------------------------
    for agg in CONFIGURED_AGGREGATES:
        
        peso = (100-MAPE_MODELS[agg][ano_mes])
        if (peso > 0):
            total += (1+data[agg+'_mult'])*peso
            peso_total +=peso      
            
    multiplier = total / peso_total
    print('>   peso_total depois de', ano_mes, "=", peso_total)

    if (peso_total > 0):
        data['revised'] = data['secomod'] * multiplier
        data['revised'] = data['revised'].where(data['revised'] > 0, 0)
        e_data = data.combine_first(e_data)

    return e_data


# ---------------------------------------------------------------------------------------
def calculate_change(column, dpg_sku_data, other_data, USER_VALIDATION_YEAR):
    
    # Define o primeiro mês do ano de validação
    str_val = str(USER_VALIDATION_YEAR*100+1)
    
    # Agrupa os dados da prev. bottom up do ano corrente e do futuro para a chave do agregado somando o secomod
    base  = dpg_sku_data.query('ano_mes >= ' + str_val).groupby([column, 'year', 'month'])['secomod'].sum()
    
    # Agrupa os dados da prev. top down do ano corrente e do futuro para a chave do agregado somando o secomod
    other = other_data.query('ano_mes >= ' + str_val).groupby([column, 'year', 'month'])['secomod'].sum()
    
    # Calcula o erro relativo entre as previsões BUP e TDW para cada mês de cada ano para cada chave dos agregados
    # Considera como o valor real as previsões TDW
    change = (other-base)/base
    
    # Cria uma dataframe com os erros calculados e preenche valores inf e NaN com zero
    change = change.to_frame().replace([np.inf, -np.inf], np.nan).fillna(0)
    change.columns = [column + '_mult']
    
    return change


# ---------------------------------------------------------------------------------------
def data_setup():

    from calix_config import run_system_paths, run_setup_fcst_flow, \
    run_setup_hyper, run_setup_dict, run_set_var_params, \
        run_scope_definition
    
    # Definição de variáveis necessárias
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()
    USER_CURRENT_MONTH = dict_hyper_lumos['default']['user_curr_month']
    USER_VALIDATION_YEAR = dict_hyper_lumos['default']['user_validation_year']
    version_code = dict_hyper_lumos['default']['version_code']

    year = USER_CURRENT_MONTH // 100
    month = USER_CURRENT_MONTH % 100
    
    ano_mes_1 = (USER_CURRENT_MONTH+1) if (month < 12) else (year+1)*100+1
    ano_mes_1_back = (USER_CURRENT_MONTH-1) if (month > 1) else (((
        year-1)*100)+(month+11))

    # Setup das datas
    last_valid_month = ano_mes_1_back
    if (last_valid_month % 100 == 0):
        last_valid_month = ((USER_CURRENT_MONTH//100)-1)*100+12
    
    user_start_validation = USER_VALIDATION_YEAR*100+1
    user_end_validation = USER_VALIDATION_YEAR*100+12

    if ((version_code == 'm') or (version_code == 'u')):
        user_end_training = min(last_valid_month, (
            USER_VALIDATION_YEAR-1)*100+12)
    else:
        user_end_training = last_valid_month

    return last_valid_month, user_start_validation, user_end_validation, \
         user_end_training, USER_VALIDATION_YEAR


# -----------------------------------------------------------------------------
class DataFile:
    
    # -------------------------------------------------------------------------
    def __init__(self, data):
    # -------------------------------------------------------------------------
    
        self.data = data
        
    # -------------------------------------------------------------------------
    def read(self):
    # -------------------------------------------------------------------------

        start = timer()
    
        df_read = self.data.reset_index().copy()

        # Configura os tipos de dados
        print ("Configurando tipos de dados...")
        # df_read.cod_sku = df_read.cod_sku.astype("int64")

        # Encontra as previsões fornecidas pelo usuário
        PREVISIONS_LIST = df_read.columns[df_read.columns.str.contains(
            'past_fcst')].to_list()
        df_read[PREVISIONS_LIST] = df_read[PREVISIONS_LIST].astype("float64")

        # Cria colunas para analises de mix de dados
        df_read['cod_lowest_level'] = df_read['cod_cliente'].astype(
            "str") + "_" + df_read['cod_sku'].astype("str")

        # Configura os índices
        self.original = df_read.drop(columns=['index'])
        
        end = timer()
        print("--> Completo! "+ "{:.2f}".format(end-start) + " s.")
        
        return self.original
    
    # -------------------------------------------------------------------------   
    def print_data(self, data, CONFIGURED_AGGREGATES) :
    # -------------------------------------------------------------------------

        print("Registros totais : ", data['year'].count())
        print("Combinações:")
        for m in np.sort(CONFIGURED_AGGREGATES):
            print("- " + m + " : " + str(len(data[m].unique().tolist())))
        print("- Lowest level: ", len(data['cod_lowest_level'].unique().tolist()))
    
    # -------------------------------------------------------------------------    
    def setup_training_data(
        self, CONFIGURED_AGGREGATES, start_ano_mes=0, fim_ano_mes=999999) :
    # -------------------------------------------------------------------------

        data = self.original.copy()

        # Remove dados anteriores ao ano definido para início do treino
        if (start_ano_mes > 0):
            data = data.query('ano_mes >= ' + str(start_ano_mes))
            print("--------------------------------------------------")
            print("Removendo dados anteriores a " + str(start_ano_mes))
            print("--------------------------------------------------")
            self.print_data(data, CONFIGURED_AGGREGATES)
        
        # ---------------------------------------------------------------------
        # Remove dados posteriores ao ano definido para fim do treino

        if (fim_ano_mes < 999999):
            data = data.query('ano_mes <= ' + str(fim_ano_mes))
            print("--------------------------------------------------")
            print("Removendo dados posteriores a " + str(fim_ano_mes))
            print("--------------------------------------------------")
            self.print_data(data, CONFIGURED_AGGREGATES)
        
        # ---------------------------------------------------------------------
        # Remove dpg_skus que não tiveram venda em NENHUM ano, NENHUM mês

        dpg_sku_complete_list_init = data['cod_lowest_level'].unique().tolist()
        len_data_init = data.shape
    
        # Df c/ soma do yhat para todo o período de dados para cada dpg_sku
        dpg_sku_for_training_compilation = data.groupby('cod_lowest_level')[
            'yhat'].sum().to_frame()
        
        # Lista c/ os dpg_skus que não têm venda: yhat igual a zero ou negativo
        dpg_sku_without_sells_list = dpg_sku_for_training_compilation[
            dpg_sku_for_training_compilation.yhat <= 0].index.get_level_values(
                'cod_lowest_level').unique().tolist()
        
        # Lista c/ os dpg_skus que possuem venda: yhat maior que zero
        dpg_sku_for_training_list = dpg_sku_for_training_compilation[
            dpg_sku_for_training_compilation.yhat > 0].index.get_level_values(
                'cod_lowest_level').unique().tolist()
        
        # Filtra o df principal p/ conter apenas os dpg_skus que possuem venda
        data = data[data['cod_lowest_level'].isin(dpg_sku_for_training_list)]

        # Printa o que foi removido
        print("--------------------------------------------------")
        print("DPG x SKU sem vendas no período de treino: ", len(
            dpg_sku_without_sells_list))
        print("--------------------------------------------------")
        self.print_data(data, CONFIGURED_AGGREGATES)

        # ---------------------------------------------------------------------
        # Remove do banco de treino dpg_skus que não tiveram venda no last_year

        last_year = data['year'].max()
        if (data.query("year == " + str(last_year))['month'].nunique() == 1):
            last_year = (last_year-1)

        # Dataframe contendo a soma do yhat para cada ano para cada dpg_sku
        dpg_sku_for_training_compilation_by_year = data.groupby(
            ['cod_lowest_level','year'])['yhat'].sum().to_frame()

        # Lista com os dpg_skus com yhat igual a zero no last_year
        no_sells_df = dpg_sku_for_training_compilation_by_year.query("yhat == 0")
        no_sells_on_the_last_year = no_sells_df.loc[
            no_sells_df.index.get_level_values('year') == last_year]
        dpg_sku_without_sells_on_the_last_year_list =\
             no_sells_on_the_last_year.index.get_level_values(
                 'cod_lowest_level').unique().tolist()

        # Lista com os dpg_skus com yhat maior que zero no last_year
        existing_sells_df =\
             dpg_sku_for_training_compilation_by_year.query('yhat > 0')
        existing_sells_on_last_year =\
             existing_sells_df.loc[existing_sells_df.index.get_level_values(
                 'year') == last_year]
        dpg_sku_for_training_list =\
             existing_sells_on_last_year.index.get_level_values(
                    'cod_lowest_level').unique().tolist()

        # Filtra o df principal p/ conter os dpg_skus c/ venda no último ano
        data = data[data['cod_lowest_level'].isin(dpg_sku_for_training_list)]

        # Printa o que foi removido
        print("--------------------------------------------------")
        print("DPG x SKU sem dados no último ano: ", len(
            dpg_sku_without_sells_on_the_last_year_list))
        print("--------------------------------------------------")
        self.print_data(data, CONFIGURED_AGGREGATES)

        # ---------------------------------------------------------------------
        # Remove do banco de treino anos de dpg_skus que não tiveram venda

        # Dataframe com agrupação do yhat no nível ['cod_lowest_level','year']
        dpg_sku_compilation_by_year =\
             data.groupby(['cod_lowest_level','year'])['yhat'].sum().to_frame()
        
        # Cria coluna para identificar pares dpg_sku/ano com yhat zerado
        dpg_sku_compilation_by_year['delete'] =\
             dpg_sku_compilation_by_year['yhat'] == 0
        dpg_sku_compilation_by_year =\
             dpg_sku_compilation_by_year.drop(columns=['yhat'])
        
        data = data.merge(
            dpg_sku_compilation_by_year, on=['cod_lowest_level','year'])
        
        # Lista linhas (indexes) com pares dpg_sku/ano com yhat zerado
        indexNames = data[ data['delete'] ].index
        
        data.drop(indexNames , inplace=True)
        data = data.drop(columns=['delete'])

        print("--------------------------------------------------")
        print("DPG x SKU registros de anos sem vendas: ", len(indexNames))
        print("--------------------------------------------------")
        self.print_data(data, CONFIGURED_AGGREGATES)

        # ---------------------------------------------------------------------
        # Remove do banco de treino anos de dpg_skus com apenas 1 mes de dados  

        # Df com contagem de qts meses possuem yhat>0 p/ cada ano p/ cada dpg_sku 
        dpg_sku_compilation_by_year =\
             data.query('yhat > 0').groupby(
                 ['cod_lowest_level','year'])['yhat'].count().to_frame() 
        dpg_sku_compilation_by_year.columns = ['meses_non_zero']
        
        # Une o df principal e o df com a coluna que conta os meses com yhat>0
        temp = data.merge(
            dpg_sku_compilation_by_year, on=['cod_lowest_level', 'year'])
        
        # Lista linhas c/ pares dpg_sku/ano que possuem yhat p/ 1 ou nenhum mês 
        indexNames = temp[ temp['meses_non_zero'] <= 1 ].index
        
        # Filtra o df p/ remover linhas c/ pares dpg_sku/ano c/ yhat zerado
        temp.drop(indexNames , inplace=True)
        data = temp.drop(columns=['meses_non_zero'])

        print("--------------------------------------------------")
        print("DPG x SKU anos com apenas 1 mes de dados: ", len(indexNames))
        print("--------------------------------------------------")
        self.print_data(data, CONFIGURED_AGGREGATES)

        # ---------------------------------------------------------------------
        # Remove do banco de treino dpg_skus com yhat < 100 no último ano

        # Dataframe com dados agrupados no yhat para o last_year
        dpg_sku_for_training_compilation = data.loc[data['year'] == last_year]
        dpg_sku_for_training_compilation =\
             dpg_sku_for_training_compilation.groupby(
                 'cod_lowest_level')['yhat'].sum().to_frame()

        # Lista com os dpg_skus com medido < 100 no último ano
        dpg_sku_without_sells_last_year_list =\
             dpg_sku_for_training_compilation[
                 dpg_sku_for_training_compilation.yhat <= 100].index.get_level_values(
                     'cod_lowest_level').unique().tolist()

        # Lista com os dpg_skus com medido > 100 no último ano
        dpg_sku_for_training_list = dpg_sku_for_training_compilation[
            dpg_sku_for_training_compilation.yhat > 100].index.get_level_values(
                'cod_lowest_level').unique().tolist()

        #  Filtra o df principal p/ remover dpg_skus com yhat<100 no último ano
        data = data[data['cod_lowest_level'].isin(dpg_sku_for_training_list)]

        print("--------------------------------------------------")
        print("DPG x SKU sem vendas no último ano (vendas < 100): ", len(
            dpg_sku_without_sells_last_year_list))
        print("--------------------------------------------------")
        self.print_data(data, CONFIGURED_AGGREGATES)

        # ---------------------------------------------------------------------
        # Zera medições negativas (devoluções) no banco de treino

        data = data.copy()
        
        # Cria coluna para identificar instâncias com yhat negativo
        data['delete'] = data['yhat'] < 0
        
        # Lista linhas (indexes) com instâncias com yhat negativo
        indexNames = data[ data['delete'] ].index
        
        # Altera o yhat colocando 0 onde é negativo
        data['yhat'] = data['yhat'].where(data['yhat'] > 0, 0)
        data.drop(['delete'], 1, inplace=True)

        print("--------------------------------------------------")
        print("DPG x SKU meses com devolução zerados/negativas: ", len(indexNames))
        print("--------------------------------------------------")
        self.print_data(data, CONFIGURED_AGGREGATES)

        # ---------------------------------------------------------------------
        # Resumo das limpezas aplicadas
        dpg_sku_complete_list_end = data['cod_lowest_level'].unique().tolist()
        len_data_end = data.shape
        print("--------------------------------------------------")
        print("Resumo da preparação dos dados de treino: ")
        print("Qtd. SKUs início:  ", str(len(dpg_sku_complete_list_init)))
        print("Qtd. SKUs fim:     ", str(len(dpg_sku_complete_list_end)))
        print("Data shape início: ", str(len_data_init))
        print("Data shape fim:    ", str(len_data_end))
        print("Anos presentes:    ", str(data['year'].unique()))
        print("--------------------------------------------------")

        self.training_data = data.copy()
        
        return self.training_data

    # ------------------------------------------------------------------------- 
    def setup_validation_data(self, aggregate, user_previsions, validation_year):
    # ------------------------------------------------------------------------- 
    # Separa um pedaço dos dados e cria espaços vazios para o futuro

        everybody_to_predict =\
             self.original.query('year <= ' + str(
                 validation_year) + '& year >= ' + str(validation_year-1))
        everybody_to_predict =\
             everybody_to_predict.groupby([aggregate,'year','month'])[
                 user_previsions + ['yhat']].agg(['sum'])
        
        # Lista com as chaves do agregado
        validation_keys_list = everybody_to_predict.index.get_level_values(aggregate).unique().tolist()
        
        # Renomeia colunas 
        everybody_to_predict.columns = user_previsions + ['yhat']

        # Cria um df vazio em que, para cada chave do agregado, tem-se x anos de espaços
        keys_list = []
        year_list = []
        month_list = []
        for key in validation_keys_list:
            keys_list +=  [key]*12*3
            year_list +=  [validation_year-1] * 12 + [validation_year] * 12 + [validation_year+1] * 12
            month_list += [m for m in range(1, 13)] + [m for m in range(1, 13)] + [m for m in range(1, 13)]
        df = pd.DataFrame({aggregate:keys_list, 'year':year_list, 'month':month_list})
        df['ano_mes'] = df['year']*100 + df['month']
        df['am'] = df['year'] + df['month']/13
        df = df.set_index([aggregate,'year','month'])
        
        # Une o df vazio criado com o corte feito o df original
        validation_data = everybody_to_predict.combine_first(df).fillna(0).copy()

        return validation_data, validation_keys_list

# ---------------------------------------------------------------------------------------
def create_empty_future(data):

    from calix_config import run_setup_fcst_flow
    dict_setup_fcst_flow, dict_setup_clst_params = run_setup_fcst_flow()

    print("Criando espaços para o futuro...")
    start = timer()

    start_date = dict_setup_fcst_flow['start_training_date']
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    fcst_horizon = dict_setup_fcst_flow['fcst_horizon']
    cycles = dict_setup_fcst_flow['number_fcst_cycles']
    
    # Calcula primeiro mês sendo previsto
    start_future_date = (start_date + relativedelta(months=1)).date()
    future_start_year = start_future_date.year
    start_am = int(start_future_date.strftime("%Y") + start_future_date.strftime("%m"))

    # Calcula último mês do horizonte sendo previsto
    end_future_date = (start_date + relativedelta(months=(fcst_horizon+cycles-1)))
    end_am = int(end_future_date.strftime("%Y") + end_future_date.strftime("%m"))
    future_end_year = end_future_date.year

    all_indexes_list = data.groupby(['cod_cliente', 'cod_sku']).sum().index.unique().tolist()
    validator = data.copy()   

    print("> Total lines in file:", len(data))
    all_indexes_list = validator.groupby(['cod_sku', 'cod_cliente']).sum().index.unique().tolist()
    print("> Total combinations:", len(all_indexes_list))

    c = 1
    tot = len(all_indexes_list)
    created_register = 0
    t_list = []
    
    for index in all_indexes_list:
        c = c+1
        l = list(index)
        for y in range(future_start_year, future_end_year+1):
            for m in range (1, 13):
                ltemp = list(index)
                ltemp.append(y*100+m)
                bindex = tuple(ltemp)
                try:
                    validator.loc[bindex]
                except:
                    t_list.append(bindex)
                    created_register = created_register + 1
                    pass
    
    print("> Registros criados: "+ str(created_register))
    dfObj = pd.DataFrame(t_list, columns = ['cod_sku','cod_cliente','ano_mes']).set_index(
                                                                                    ['cod_sku', 'cod_cliente', 'ano_mes'])

    validator = validator.set_index(['cod_sku', 'cod_cliente', 'ano_mes'])
    result = validator.combine_first(dfObj).fillna(0)
    print("> Total lines in file:", len(result))
    all_indexes_list = result.groupby(['cod_sku', 'cod_cliente']).sum().index.unique().tolist()
    print("> Total combinations:", len(all_indexes_list))
    
    # Refazendo as colunas de 'timestamp' e 'cod_lowest_level' para os espaços vazios criados
    result['timestamp'] = result.index.get_level_values('ano_mes').astype("str")
    result['timestamp'] = result.timestamp.apply(lambda x: datetime.strptime(x, '%Y%m'))
    result['cod_lowest_level'] = result.index.get_level_values('cod_cliente').astype("str") + "_" + result.index.get_level_values('cod_sku').astype("str")
    
    result = result.reset_index()

    end = timer()
    print("--> Completo! "+ "{:.2f}".format(end-start) + " s.")
    print("--------------------------------------------------")
    
    return result