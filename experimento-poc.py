#!/usr/bin/python
# -*- coding: latin-1 -*-
import os
from multiprocessing import Pool
#os.system('conda activate ./calix')
#os.system('conda activate calix')
#os.system('conda install pandas')
#os.system('conda install numpy')
#os.system('conda install category_encoders')
#os.system('conda install fastparquet')
#os.system('conda install -c conda-forge optuna')
#os.system('conda install -c conda-forge fbprophet')
#os.system('conda install catboost')
#os.system('conda install lightgbm')
#os.system('conda install tslearn')
from catboost import CatBoostRegressor
from fbprophet import Prophet
import lightgbm as lgb
os.system('pip install python-dateutil --upgrade')
import pandas as pd
import numpy as np
#from calix_load_data2 import run_load_data2
#from calix_preprocess2 import run_preprocess2
#os.system('conda install numpy')
#
#os.system('conda install category_encoders')
#os.system('conda install fastparquet -y')
#os.system('conda install -c conda-forge optuna -y')
#os.system('conda install -c conda-forge fbprophet --yes')
#os.system('conda install catboost -y')
#os.system('conda install lightgbm')
#os.system('conda install tslearn --user')
import warnings
from datetime import datetime
from dateutil.relativedelta import relativedelta
#import lightgbm as lgb
#from catboost import CatBoostRegressor
#from fbprophet import Prophet

#import plotly as plt
#import plotly.express as px
import random
#from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from timeit import default_timer as timer
import sys

import optuna
from optuna.visualization import plot_contour
from optuna.visualization import plot_edf
from optuna.visualization import plot_intermediate_values
from optuna.visualization import plot_optimization_history
from optuna.visualization import plot_parallel_coordinate
from optuna.visualization import plot_param_importances
from optuna.visualization import plot_slice


from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MaxAbsScaler
from sklearn.preprocessing import RobustScaler




from user_config import USER_PATH

warnings.filterwarnings("ignore")  # avoid printing out absolute paths
os.chdir("../../..")

cliente_isystems = 'FABER'

#teste = run_load_data2(cliente_isystems)
#prep = run_preprocess2(cliente_isystems,teste)


def run_load_data2(cliente_isystems):
    #print(USER_PATH)
    #Referenciando local onde fica armazenado o código CONFIG do cliente
    print(USER_PATH + cliente_isystems + "/SETUP/")
    sys.path.append(USER_PATH + cliente_isystems + "/SETUP/")

    from calix_config import run_system_paths, run_setup_fcst_flow, \
        run_setup_hyper, run_setup_dict, run_set_var_params, \
            run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()

    # Leitura da tabela fato e helpers
    df = pd.read_parquet(path_curated + '/tab_fato_appended.parquet')
    df_meta_prod = pd.read_parquet(path_curated + '/tab_meta_prod.parquet')
    df_meta_prod.drop(['as_of', 'cliente_isystems'], 1, inplace=True)
    df_meta_custom = pd.read_parquet(path_curated + '/tab_meta_custom.parquet')
    df_meta_custom.drop(['as_of', 'cliente_isystems'], 1, inplace=True)

    try:
        df_extra_feat1d_sku = pd.read_parquet(path_curated + '/df_extra_feat1d_sku.parquet')
        df_extra_feat1d_sku.drop(['as_of','cliente_isystems'], 1, inplace=True)
        df = pd.merge(df, df_extra_feat1d_sku, on='cod_sku', how='left')
    except Exception:
        pass

    df[['cod_sku']] = df[['cod_sku']].applymap(str)

    # Une os dados auxiliares à tabela fato
    df = pd.merge(df, df_meta_prod, on='cod_sku', how='left')
    df = pd.merge(df, df_meta_custom, on='cod_cliente', how='left')

    # Corrige o dataframe caso merges desnecessários tenham sido feitos
    to_drop = df.columns[df.columns.str.contains("_x")]
    df.drop(to_drop, 1, inplace=True)
    df.columns = df.columns.str.rstrip("_y")

    # Definindo o escopo de execução do modelo
    #out_of_scope = ['Não Atribuído','NO FORECAST']
    df_prot = df.copy()
    #df_prot = df[df['marca']=='BRAHMA'].reset_index(drop=True)
    #df_prot = df[df['feat1d_sku1']=='Mainstream'].reset_index(drop=True)
    #df_prot = df_prot[~df_prot['prod_agg5'].isin(out_of_scope)]
    #df_prot = df_prot[df_prot['feat1d_sku2']!=1.0]
    del(df)

    df_prot['month'] = df_prot['timestamp'].dt.month
    df_prot['year'] = df_prot['timestamp'].dt.year

    df_prot[['month']] = df_prot[['month']].applymap(str)
    df_prot[['year']] = df_prot[['year']].applymap(str)

    return df_prot
def run_preprocess2(cliente_isystems, df_prot):

# Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "/SETUP/")

    from calix_config import run_system_paths, run_setup_fcst_flow, \
    run_setup_hyper, run_setup_dict, run_set_var_params, \
        run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    dict_lowest_level, dict_meta_prod, dict_meta_custom, dict_meta_calend, \
    dict_fato, dict_extra_feat1d_sku, dict_extra_feat1d_custom, \
        dict_extra_feat1d_time, dict_extra_feat3d = run_setup_dict()
    dict_setup_fcst_flow, dict_setup_clst_params = run_setup_fcst_flow()

    RUN_LUMOS_PREPRO = dict_setup_fcst_flow['run_lumos_prepro']
    COMPLETE_TS = dict_setup_fcst_flow['complete_ts_lags']

    # Zerando valores de venda negativos caso existam
    df_prot[var_target[0]] = df_prot[var_target[0]].apply(lambda x: 0 if x<0 else x)

# Filtrando os dados conforme escopo do cliente
    df_prot = run_scope_definition(df_prot)

# Torna opcional a geração dos lags e o preenchimento dos buracos na TS
    if (COMPLETE_TS):
        # df de variáveis numéricas
        int_features = df_prot.select_dtypes('int').columns.to_list()
        float_features = df_prot.select_dtypes('float').columns.to_list()
        num_features = int_features + float_features
        df_num = df_prot[['cod_cliente','cod_sku','timestamp']+num_features]

        # df de variáveis categóricas
        cat_features = df_prot.select_dtypes('object').columns.to_list()
        df_cat = df_prot[cat_features]

        # Criação de df com séries temporais completas (mês a mês)
        start = df_num['timestamp'].min()
        end = df_num['timestamp'].max()+relativedelta(months=+1)
        timestamp_list = pd.date_range(start, end, freq='1M')-pd.offsets.MonthBegin(1)
        timestamp_list_size = len(timestamp_list)
        # variável dummy para forçar séries a terem o tamanho desejado
        dummy_df = pd.DataFrame({'timestamp':timestamp_list, 'cod_sku':'dummy','cod_cliente':'dummy'})
        for feature in num_features:
            dummy_df[feature] = 0

        df_num = pd.concat([df_num, dummy_df])
        full_series_df = pd.pivot_table(df_num, values=num_features, index='timestamp', columns=['cod_sku', 'cod_cliente'], aggfunc=np.sum, fill_value=0)
        # extrai cada variável numérica do full_series_df
        df_aux_list = []
        for feature in num_features:
            df_aux = pd.melt(full_series_df[feature], ignore_index=False, value_name=feature).reset_index()
            df_aux.set_index(['cod_sku', 'cod_cliente', 'timestamp'], inplace=True)
            df_aux_list.append(df_aux.copy())

        full_series_df = pd.concat(df_aux_list, axis=1).reset_index()

        full_series_df = full_series_df[full_series_df['cod_sku']!='dummy']
        full_series_df['month'] = full_series_df['timestamp'].dt.month.astype(str)
        full_series_df['year'] = full_series_df['timestamp'].dt.year.astype(str)

        # Voltando as infos do df original
        full_series_df = full_series_df.set_index(['cod_sku','cod_cliente'])
        df_full = pd.merge(full_series_df, df_cat.drop(columns=['month', 'year']), on=['cod_sku','cod_cliente'], how='right')

        df_prot = df_full.reset_index()
        df_prot = df_prot.drop(columns=['index'])

        # fill categorical missing data
        cat_features_missing_data = {}
        cat_features = df_prot.select_dtypes('object').columns.to_list()
        for feature in cat_features:
            if df_prot[feature].isna().sum() > 0:
                cat_features_missing_data[feature] = 'OUTRO'
        if len(cat_features_missing_data) > 0:
            df_prot.fillna(cat_features_missing_data, inplace=True)
        df_prot = df_prot.drop_duplicates()
        if not dict_setup_fcst_flow['ts_models']:
            # Criando variável com Lag de Vendas
            lag_list = ['12', '6']
            for L in lag_list:
                df_prot['yhat_lag'+L] = df_prot.groupby(['cod_sku','cod_cliente'])['yhat'].shift(int(L))
                df_prot['yhat_lag'+L].fillna(0,inplace=True)

    # Eliminando colunas com excessivos nulos
    df_prot = df_prot[df_prot.columns[df_prot.isnull().mean() < 0.8]]
    print("Limpeza de nulos concluída!")

    # Roda o pré-processamento do Lumos
    if (RUN_LUMOS_PREPRO):
        print("\nIniciando pré-processamentos Lumos...")
        original, data_to_training, validation_data_per_aggregate, \
            validation_keys_per_aggregate = run_lumos_prepro(df_prot)

    try:
        # quando o preprocessing Lumos é executado
        return original, data_to_training, validation_data_per_aggregate, \
        validation_keys_per_aggregate
    except:
        # quando apenas o preprocessing default é executado
        return df_prot


def wmape (actual, pred):
    
    actual, pred = np.array(actual), np.array(pred)
    eabs = abs(actual-pred).sum()
    actual = actual.sum()
    wmape = eabs/actual*100
    return wmape

def shift_feature(df,lag_size,feature):
    transf_df = df.copy()
    print(transf_df.isna().sum())

    transf_df['venda_fcst'] = np.where(transf_df[['timestamp']]<start_cycle_date,transf_df[['yhat']],transf_df[[feature]])
    transf_df.sort_values(['cod_sku']+['cod_cliente']+['timestamp'], inplace=True)

    # configure lag columns
    lag_columns = ['lag_'+str(i) for i in range(1, lag_size + 1)]
    for lag in range(1, lag_size + 1):
        transf_df['lag_'+str(lag)] = transf_df.groupby(['cod_sku','cod_cliente'])['venda_fcst'].shift(lag)
    transf_df.fillna(0,inplace=True)
    #transf_df.dropna(inplace=True)
    return transf_df


def run_log_transform(df):
    df[df.select_dtypes(float).columns] = df.select_dtypes(float).applymap(lambda x: np.log1p(x))
    return df

def run_inverse_log_transform(df):
    df[df.select_dtypes(float).columns] = df.select_dtypes(float).applymap(lambda x: np.exp1m(x))
    return df


def run_lgbm3(loop,latest_param_lgb,xtr, ytr, xval, yval, xts, yts):

    # Definir index como cod_sku & cod_cliente
    xtr.set_index(['cod_sku','cod_cliente'],inplace=True)
    xval.set_index(['cod_sku','cod_cliente'],inplace=True)
    xts.set_index(['cod_sku','cod_cliente'],inplace=True)

    # Df para train-test split no optuna
    xtr_full = xtr.copy()
    xtr.drop(columns='timestamp',inplace=True)
    xval.drop(columns='timestamp',inplace=True)
    xts.drop(columns='timestamp',inplace=True)

    if loop in loop_list:

        # Listar vars categoricas
        cat_features = xtr.select_dtypes('object').columns.to_list()

        # Conversao p/ category (req do LGBM)
        for c in cat_features:
            xtr[c] = xtr[c].astype('category')
            xval[c] = xval[c].astype('category')
            xts[c] = xts[c].astype('category')  

        # Formando objetos Dataset
        train_lgb = lgb.Dataset(xtr,label=ytr.ravel(),categorical_feature=cat_features, free_raw_data=False)
        val_lgb = lgb.Dataset(xval,label=yval.ravel(),categorical_feature=cat_features, free_raw_data=False)


        import optuna

        # FYI: Objective functions can take additional arguments
        # (https://optuna.readthedocs.io/en/stable/faq.html#objective-func-additional-args).
        def objective(trial):

            def wmape (actual, pred):
                actual, pred = np.array(actual), np.array(pred)
                eabs = abs(actual-pred).sum()
                actual = actual.sum()
                wmape = eabs/actual*100
                return wmape

            param = {
                'num_boost_round':trial.suggest_int('num_boost_round',30,200),
                'learning_rates':trial.suggest_float('learning_rates',0.01,1),
                'verbosity': -1,
                'metric': 'l2',
                'categorical_feature': 'auto',
                'boosting': trial.suggest_categorical('boosting',['gbdt','rf','dart']),
                #'lambda_l1': trial.suggest_loguniform('lambda_l1', 1e-8, 10.0),
                #'lambda_l2': trial.suggest_loguniform('lambda_l2', 1e-8, 10.0),
                'num_leaves': trial.suggest_int('num_leaves', 2, 256),
                'max_depth': trial.suggest_int('max_depth', 200, 500),
                #'feature_fraction': trial.suggest_uniform('feature_fraction', 0.4, 1.0),
                'bagging_fraction': trial.suggest_uniform('bagging_fraction', 0.4, 1.0),
                'bagging_freq': trial.suggest_int('bagging_freq', 1, 100),
                #'min_child_samples': trial.suggest_int('min_child_samples', 5, 100),
                'seed':123,
            }

            mdl_lgb = lgb.train(param, train_lgb)
            print("Treino LightGBM concluído!")

            p = mdl_lgb.predict(xval)
            wmape = wmape(p,yval)
            return wmape

        if __name__ == "__main__":
            study = optuna.create_study(direction="minimize",pruner=optuna.pruners.MedianPruner())
            study.optimize(objective, n_trials=100)

            print("Number of finished trials: {}".format(len(study.trials)))

            print("Best trial:")
            trial = study.best_trial

            print("  Value: {}".format(trial.value))

            print("  Params: ")
            for key, value in trial.params.items():
                print("    {}: {}".format(key, value))

        params_lgb = study.best_trial.params
        latest_param_lgb = params_lgb

    else:
        pass

    new_xtrain = xtr.append(xval)
    new_ytrain = np.concatenate([ytr, yval])

    cat_features = new_xtrain.select_dtypes('object').columns.to_list()
    print('Cat Features1: ',cat_features)

    for c in cat_features:
        new_xtrain[c] = new_xtrain[c].astype('category')
        xts[c] = xts[c].astype('category')

    #cat_features = new_xtrain.select_dtypes('category').columns.to_list()
    #print('Cat Features2: ',cat_features)

    # Formando objetos Dataset
    train_lgb = lgb.Dataset(new_xtrain,label=new_ytrain.ravel(),categorical_feature=cat_features, free_raw_data=False)
    tst_lgb = lgb.Dataset(xts,label=yts.ravel(),categorical_feature=cat_features, free_raw_data=False)    

    mdl_lgb = lgb.train(latest_param_lgb,train_lgb)
    print("Treino LightGBM concluído!")

    p = mdl_lgb.predict(xts)
    p = np.where(p<0,0,p)
    print("Previsão BOTTOM UP via LightGBM concluída!")

    model_params = latest_param_lgb
    model_name = 'light_gbm'

    return p,mdl_lgb,model_params,model_name



def run_catboost2(loop,latest_param_cat,xtr, ytr, xval, yval, xts, yts):

    # Definir index como cod_sku & cod_cliente
    xtr.set_index(['cod_sku','cod_cliente'],inplace=True)
    xval.set_index(['cod_sku','cod_cliente'],inplace=True)
    xts.set_index(['cod_sku','cod_cliente'],inplace=True)

    # Df para train-test split no optuna
    xtr_full = xtr.copy()
    xtr.drop(columns='timestamp',inplace=True)
    xval.drop(columns='timestamp',inplace=True)
    xts.drop(columns='timestamp',inplace=True)

    if loop in loop_list:

        # Listar vars categoricas
        cat_features = xtr.select_dtypes('object').columns.to_list()

        # Conversao p/ category (req do LGBM)
        for c in cat_features:
            xtr[c] = xtr[c].astype('category')
            xval[c] = xval[c].astype('category')
            xts[c] = xts[c].astype('category')  

        # initialize Pool
        train_pool = Pool(xtr,ytr,cat_features=cat_features)
        val_pool = Pool(xval,cat_features=cat_features) 

        # FYI: Objective functions can take additional arguments
        # (https://optuna.readthedocs.io/en/stable/faq.html#objective-func-additional-args).
        def objective(trial):

            def wmape (actual, pred):
                actual, pred = np.array(actual), np.array(pred)
                eabs = abs(actual-pred).sum()
                actual = actual.sum()
                wmape = eabs/actual*100
                return wmape

            param = {'n_estimators': trial.suggest_int('n_estimators',100,300),
                     'learning_rate': trial.suggest_float('learning_rate',0.01,1),
                     'silent':True,
                     'l2_leaf_reg': trial.suggest_int('l2_leaf_reg',10,30),
                     #'max_depth': trial.suggest_int('max_depth',10,100),
                     'langevin': True,
                     #'use_best_model':True,
                     'eval_metric':'RMSE',
                     #'loss_function':'RMSE',
                     'random_seed':123,
                     'loss_function': trial.suggest_categorical('loss_function',['RMSE','MAE'])}


            mdl_cat = CatBoostRegressor(**param)
            mdl_cat.fit(train_pool,eval_set=[(xval, yval)],silent=True,plot=False,early_stopping_rounds=100)
            print("Treino Catboost concluído!")

            p = mdl_cat.predict(val_pool)
            wmape = wmape(p,yval)
            return wmape

        if __name__ == "__main__":
            study = optuna.create_study(direction="minimize",pruner=optuna.pruners.MedianPruner())
            study.optimize(objective, n_trials=100, timeout=600)

            print("Number of finished trials: {}".format(len(study.trials)))

            print("Best trial:")
            trial = study.best_trial

            print("  Value: {}".format(trial.value))

            print("  Params: ")
            for key, value in trial.params.items():
                print("    {}: {}".format(key, value))

        params_cat = study.best_trial.params

        plot_optimization_history(study)
        plot_param_importances(study)
        latest_param_cat = params_cat

    else:
        pass      

    new_xtrain = xtr.append(xval)
    new_ytrain = np.concatenate([ytr, yval])

    cat_features = new_xtrain.select_dtypes('object').columns.to_list()

    for c in cat_features:
        new_xtrain[c] = new_xtrain[c].astype('category')
        xts[c] = xts[c].astype('category')

    cat_features = new_xtrain.select_dtypes('category').columns.to_list()

    # Formando objetos Dataset
    new_train_pool = Pool(new_xtrain,new_ytrain,cat_features=cat_features)
    test_pool = Pool(xts,cat_features=cat_features)  

    mdl_cat = CatBoostRegressor(**latest_param_cat)
    mdl_cat.fit(new_train_pool,silent=True,plot=False)
    print("Treino CatBoost concluído!")
    p = mdl_cat.predict(test_pool)
    p = np.where(p<0,0,p)
    print("Previsão BOTTOM UP via Catboost concluída!")

    model_params = latest_param_cat
    model_name = 'catboost'

    return p,mdl_cat,model_params,model_name   






#def run_prophet(prep,fcst_hrz):
    #df_prophet = prep[['timestamp','yhat']].groupby('timestamp')['yhat'].sum().reset_index()
   # df_prophet.rename({'timestamp':'ds','yhat':'y'},axis=1,inplace=True)
    #m = Prophet()
    #m.fit(df_prophet)
    #print('Treino Prophet concluído!')    
    #future = m.make_future_dataframe(periods=fcst_hrz+3, freq='MS')
    #fcst = m.predict(future)
    #fcst = fcst[['ds','yhat']].rename({'ds':'timestamp','yhat':'fcst_top'},axis=1)
    #return fcst

 #FUNÇÃO DE PRÉ PROCESSAMENTO



def prep_dados(prep,temp,start_cycle_date,fcst_hrz,delta,shifts,fcst_feature,log_transform,feature_scaling):
    prep2 = prep.copy()
    if loop==0:
        prep2=shift_feature(prep2,shifts,'past_fcst1')
        prep2.fillna(0,inplace=True)
        prep2.drop(['venda_fcst'],axis=1,inplace=True)

    else:
        temp = df_results[df_results['as_of']==(start_cycle_date-delta)]
        temp = temp[['timestamp','cod_sku','cod_cliente',fcst_feature]]
        prep2 = prep2.merge(temp,'left',on=['timestamp','cod_sku','cod_cliente'])
        prep2=shift_feature(prep2,shifts,fcst_feature)
        prep2.fillna(0,inplace=True)
        prep2.drop(['venda_fcst'],axis=1,inplace=True)

    #Separação das bases de dados em Treino, Validação e Teste
    print('Separando Bases de Treino, Validação e Teste')
    train = prep2[prep2['timestamp']<start_val_date]
    val = prep2[(prep2['timestamp']>start_val_date)&(prep2['timestamp']<=start_cycle_date)]
    test = prep2[(prep2['timestamp']>start_cycle_date)&(prep2['timestamp']<=(start_cycle_date+relativedelta(months=fcst_hrz)))]

    test_cp = test.copy()
    train2 = train.copy()

    print('Preprocessamento das Bases')


    if log_transform:
        train = run_log_transform(train)
        val = run_log_transform(val)
        test = run_log_transform(test)
    else:
        pass

    #Preparando os datasets para ingestão nos modelos
    xtr,xval,xts = train.drop('yhat',axis=1),val.drop('yhat',axis=1),test.drop('yhat',axis=1)
    ytr,yval,yts = train.yhat.values,val.yhat.values,test.yhat.values

    if feature_scaling:
        scaler = get_scaler(scaler_method)
        xtr[xtr.select_dtypes(float).columns] = scaler.fit_transform(xtr.select_dtypes(float))
        xval[xval.select_dtypes(float).columns] = scaler.transform(xval.select_dtypes(float))
        xts[xts.select_dtypes(float).columns] = scaler.transform(xts.select_dtypes(float))

        ytr = scaler.fit_transform(ytr.reshape(-1,1))
        yval = scaler.transform(yval.reshape(-1,1))
        yts = scaler.transform(yts.reshape(-1,1)) 
    else:
        pass

    return xtr,xval,xts,ytr,yval,yts,test_cp,train2


if __name__ == '__main__':
    #time
    nome_experimento = 'MULTIMODEL + OPTUNA [0,3,6,9] + LOG TRANSFORM + SHIFT TS 24M + PROPHET TOP'

    start_cycle_date = '2018-09-01'
    number_fcst_cycles = 15
    fcst_hrz = 6
    validation_window = 3
    sliding_window = 1

    loop_list=[0,3,6,9]
    log_transform=True
    feature_scaling = False
    scaler_method = 'robust'

    loop = 0
    latest_param_cat={}
    latest_param_lgb={'categorical_feature': 'auto'}

    current_datetime = datetime.now()
    start_cycle_date = datetime.strptime(start_cycle_date, '%Y-%m-%d')
    end_cycle_date = start_cycle_date+relativedelta(months=number_fcst_cycles)
    start_val_date = start_cycle_date-relativedelta(months=validation_window)
    temp = pd.DataFrame()
    df_results = pd.DataFrame()
    df_results_temp = pd.DataFrame()
    delta = relativedelta(months=sliding_window)
    teste = run_load_data2(cliente_isystems)
    prep = run_preprocess2(cliente_isystems,teste)
    while start_cycle_date < end_cycle_date:
        print('Início do ciclo ',loop,' de Previsão em: ',start_cycle_date)

        xtr,xval,xts,ytr,yval,yts,test_cp,train2 = prep_dados(prep,temp,start_cycle_date,fcst_hrz,delta,24,'p_lgb',log_transform,feature_scaling)
        p_lgb,mdl_lgb,model_params_lgb,model_name_lgb = run_lgbm3(loop,latest_param_lgb,xtr,ytr,xval,yval,xts,yts)

        xtr,xval,xts,ytr,yval,yts,test_cp,train2 = prep_dados(prep,temp,start_cycle_date,fcst_hrz,delta,24,'p_cat',log_transform,feature_scaling)
        p_cat,mdl_cat,model_params_cat,model_name_cat = run_catboost2(loop,latest_param_cat,xtr,ytr,xval,yval,xts,yts)

        print('Previsão gerada no ciclo de: ',start_cycle_date)

        df_results_temp = test_cp
        df_results_temp['cliente_isystems'] = cliente_isystems
        df_results_temp['as_of'] = start_cycle_date
        df_results_temp['p_cat'] = p_cat
        df_results_temp['p_lgb'] = p_lgb

        if log_transform:
            df_results_temp[['p_cat']] = df_results_temp[['p_cat']].applymap(lambda x: np.expm1(x))
            df_results_temp[['p_lgb']] = df_results_temp[['p_lgb']].applymap(lambda x: np.expm1(x))
        else:
            pass

        if feature_scaling:
            df_results_temp[['p_cat']] = scaler.inverse_transform(df_results_temp[['p_cat']])
            df_results_temp[['p_lgb']] = scaler.inverse_transform(df_results_temp[['p_lgb']])

        else:
            pass

        #fcst = run_prophet(train2,fcst_hrz)
        #df_results_temp = df_results_temp.merge(fcst,'left',on='timestamp')
        temp_agg = df_results_temp.groupby('timestamp')['p_cat','p_lgb'].sum().reset_index()
        temp_agg.rename({'p_cat':'p_cat_agg','p_lgb':'p_lgb_agg'},axis=1,inplace=True)
        df_results_temp = df_results_temp.merge(temp_agg,'left',on='timestamp')
        df_results_temp['p_cat_share'] = df_results_temp['p_cat']/df_results_temp['p_cat_agg']
        df_results_temp['p_lgb_share'] = df_results_temp['p_lgb']/df_results_temp['p_lgb_agg']
        df_results_temp['p_cat_rec'] = df_results_temp['p_cat_share']*df_results_temp['fcst_top']
        df_results_temp['p_lgb_rec'] = df_results_temp['p_lgb_share']*df_results_temp['fcst_top']

        df_results_temp['p_cat_rec'] = df_results_temp['p_cat_rec'].fillna(0)
        df_results_temp['p_lgb_rec'] = df_results_temp['p_lgb_rec'].fillna(0)


        df_results_temp.drop(['p_cat_share','p_lgb_share','p_cat_agg','p_lgb_agg','fcst_top'],axis=1,inplace=True)

        df_results_temp['lag'] = ((df_results_temp['timestamp']-df_results_temp['as_of'])/np.timedelta64(1, 'M'))        
        df_results_temp['lag'] = np.round(df_results_temp['lag'])

        print('--------------------------------------------------------------')
        print('WMAPE CATBOOST CALIX: ',wmape(df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].yhat,df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].p_cat))
        print('WMAPE LIGHTGBM CALIX: ',wmape(df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].yhat,df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].p_lgb))
        print('--------------------------------------------------------------')
        print('WMAPE CATBOOST CALIX REC: ',wmape(df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].yhat,df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].p_cat_rec))
        print('WMAPE LIGHTGBM CALIX REC: ',wmape(df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].yhat,df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].p_lgb_rec))    
        print('WMAPE CLIENTE: ',wmape(df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].yhat,df_results_temp[(df_results_temp['year']=='2019')&(df_results_temp['lag']==1)].past_fcst1))


        start_cycle_date += delta
        start_val_date += delta
        loop +=1

        df_results = df_results.append(df_results_temp)
    
    



    
    #Salvando arquivos de resultados
    path = path_outputs
    day = str('00'+str(current_datetime.day))
    day = day[-2:]
    month = str('00'+str(current_datetime.month))
    month = month[-2:]
    hour = str('00'+str(current_datetime.hour))
    hour = hour[-2:]
    minute = str('00'+str(current_datetime.minute))
    minute = minute[-2:]

    # GERAÇÃO DE CÓDIGO RANDOMICO PARA CRIAÇÃO DE ID DE TESTE
    test_id_random = random.randint(10000, 99999)
    test_id = str(current_datetime.year)+month+day+hour+minute+'_'+str(test_id_random)
    print("\nTEST_ID: ", str(test_id))

    print('---------------------------------------RESULTADOS FINAIS----------------------------------------------------------')
    print('WMAPE CATBOOST CALIX: ',wmape(df_results[(df_results['year']=='2019')&(df_results['lag']==1)].yhat,df_results[(df_results['year']=='2019')&(df_results['lag']==1)].p_cat))
    print('WMAPE LIGHTGBM CALIX: ',wmape(df_results[(df_results['year']=='2019')&(df_results['lag']==1)].yhat,df_results[(df_results['year']=='2019')&(df_results['lag']==1)].p_lgb))
    print('-----------------APLICANDO RECONCILIAÇÃO--------------------------------------------------------------------------')
    print('WMAPE CATBOOST CALIX REC: ',wmape(df_results[(df_results['year']=='2019')&(df_results['lag']==1)].yhat,df_results[(df_results['year']=='2019')&(df_results['lag']==1)].p_cat_rec))
    print('WMAPE LIGHTGBM CALIX REC: ',wmape(df_results[(df_results['year']=='2019')&(df_results['lag']==1)].yhat,df_results[(df_results['year']=='2019')&(df_results['lag']==1)].p_lgb_rec))
    print('WMAPE CLIENTE: ',wmape(df_results[(df_results['year']=='2019')&(df_results['lag']==1)].yhat,df_results[(df_results['year']=='2019')&(df_results['lag']==1)].past_fcst1))

    # ARQUIVO FINAL DEVE CONTER O ID DE TESTE + NOME DO ARQUIVO + DATA E HORA DA EXECUÇÃO

    file_name = "id_"+str(test_id)+'_calix_mod_resultados_'+cliente_isystems+'.parquet'
    path = path_outputs+'/LAB/'
    output_file = os.path.join(path,file_name)
    df_results['test_id'] = test_id 
    df_results['test_desc'] = nome_experimento

    df_results.to_parquet(output_file,index=False) 
  
   