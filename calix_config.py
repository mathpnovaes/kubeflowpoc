import numpy as np
import pandas as pd

from user_config import USER_PATH


## CONFIGURAÇÃO DO CLIENTE FRAS-LE

# -----------------------------------------------------------------------------
def run_system_paths ():
    # Os caminhos de entrada serão customizados por Cliente enquanto os caminhos de saída deverão ser padronizados
    path_raw = USER_PATH + "FRAS-LE/INPUTS/raw"
    path_curated = USER_PATH + "FRAS-LE/INPUTS/curated"
    path_outputs = USER_PATH + "FRAS-LE/OUTPUTS"
    
    return path_raw, path_curated, path_outputs


# -----------------------------------------------------------------------------
def run_setup_fcst_flow ():
    dict_setup_fcst_flow = {
        'start_training_date': '2018-09-01',
        'fcst_horizon': 6,
        'number_fcst_cycles': 16,
        'sliding_window': 1,
        'model_top': 'Prophet',
        'cluster_level': ['cod_sku', 'cod_cliente'],
        'cluster_pipelines': False,
        'run_lumos_prepro': False,
        'complete_ts_lags': False,
        'cluster_dimensions':['clst_ABC_Class'],
        'cluster_dim_values':['C'],
        'optuna_optimization':False,
        'dtw_clustering':[],
        'fill_lag_fcst':False,
        'ts_models':True
    }

    dict_setup_clst_params = {'default':{'model_bup': 'lightgbm'},
    }
    return dict_setup_fcst_flow,dict_setup_clst_params

# -----------------------------------------------------------------------------
def run_setup_hyper():
    dict_hyper_lumos = {'default':{
        'user_validation_year': 2021,
        'user_curr_month': 202103, # duplicado, dar um jeito de usar o start_training_date futuramente
        'geo': 'RJ_ES', #['RJ_ES', 'OUTROS', 'MG', 'CO', 'NE', 'NO', 'SP', 'SUL'],
        'dpg_sku_params': [0.0283203125, 0.330078125, -0.0009765625],
        'oper_weight': 0.5,
        'diff_weighted_average': 0.5,
        'configured_agg': ['tipo_produto', 'retornab', 'tipo_dpg', 'estado', 'comercial', 'marca', 'embalagem', \
            'cod_dpg', 'submarca', 'cod_produto']  # talvez eu tenha que mudar pros nomes genéricos 'prod_agg1'
    }}

    dict_hyper_catboost = {'default':{
        #'nan_mode':'Min',
        'eval_metric':'RMSE',
        'iterations':500,
        #'sampling_frequency':'PerTree',
        #'leaf_estimation_method':'Newton',
        #'grow_policy':'SymmetricTree',
        #'penalties_coefficient':1,
        #'boosting_type':'Plain',
        #'model_shrink_mode':'Constant',
        #'feature_border_type':'GreedyLogSum',
        'l2_leaf_reg':10,
        'langevin': True,
        #'random_strength':0.20000000298023224,
        #'rsm':1,
        #'boost_from_average':True,
        #'model_size_reg':0.5,
        #'subsample':0.800000011920929,
        'use_best_model':False,
        'random_seed':0,
        #'depth':10,
        #'posterior_sampling':False,
        #'border_count':254,
        'cat_features': ['prod_agg4','prod_agg5','prod_agg6','clst_ABC_Class','clst_XYZ_Class','clst_lifecycle'],
        #'sparse_features_conflict_fraction':0,
        #'leaf_estimation_backtracking':'AnyImprovement',
        #'best_model_min_trees':1,
        #'model_shrink_rate':0,
        #'min_data_in_leaf':1,
        'loss_function':'MAE',
        'learning_rate':0.01
        #'score_function':'Cosine',
        #'task_type':'CPU',
        #'leaf_estimation_iterations':1,
        #'bootstrap_type':'MVS',
        #'max_leaves':1024
        },
        'C':{'loss_function':'MAE'}
        }

    dict_hyper_lgbm = {'default':{
        'params':'params',
        #'train_set':'train_set',
        'num_boost_round':100,
        'valid_sets':None,
        'valid_names':None,
        'fobj':None,
        'feval':None,
        'init_model':None,
        #'feature_name':'auto',
        #'categorical_feature':'auto',
        'early_stopping_rounds':None,
        'evals_result':None,
        'verbose_eval':True,
        'learning_rates':None,
        'keep_training_booster':False,
        'callbacks':None,
    }}
    return dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm    

# -----------------------------------------------------------------------------
def run_setup_dict ():
    dict_lowest_level = {
        'codigo_material': 'cod_sku',
        'cod_cliente_escritorio': 'cod_cliente',
        'data': 'timestamp', 
        'past_fcst1':'past_fcst1'
    }
    
    dict_meta_prod = {
        'codigo_material':'cod_sku',
        'PRODH_N3':'prod_agg1',
        'PRODH_N4':'prod_agg2',
        '':'prod_agg3',
        '':'prod_agg4',
        '':'prod_agg5',
        '':'prod_agg6',
        '':'prod_agg7',
        '':'prod_agg8',
        '':'prod_agg9',
        '':'prod_agg10',
    }

    dict_meta_custom = {
        'cod_cliente':'cod_cliente',
        '':'custom_agg1',
        '':'custom_agg2',
        '':'custom_agg3',
        '':'custom_agg4',
        '':'custom_agg5',
        '':'custom_agg6',
        '':'custom_agg7',
        '':'custom_agg8',
        '':'custom_agg9',
        '':'custom_agg10',
    }

    dict_meta_calend = {
        'dia':'date_agg1',
        'dia_da_semana':'date_agg2',
        'dia_do_ano':'date_agg3',
        'semana':'date_agg4',
        'semana_do_ano':'date_agg5',
        'mes':'date_agg6',
        'trimestre':'date_agg7',
        'semestre':'date_agg8',
        'ano':'date_agg9',
        'total_data':'date_agg10'
    }

    dict_fato = {
        'quantidade':'yhat',
        '':'past_fcst1',
        '':'past_fcst2'
    }

    dict_extra_feat1d_sku = {
        '':'cod_sku',
        '':'feat1d_sku1',
        '':'feat1d_sku2',
        '':'feat1d_sku3',
        '':'feat1d_sku4',
        '':'feat1d_sku5',
        '':'feat1d_sku6',
        '':'feat1d_sku7',
        '':'feat1d_sku8',
        '':'feat1d_sku9',
        '':'feat1d_sku10'  
    }

    dict_extra_feat1d_custom = {
        '':'cod_cliente',
        '':'feat1d_custom1',
        '':'feat1d_custom2',
        '':'feat1d_custom3',
        '':'feat1d_custom4',
        '':'feat1d_custom5',
        '':'feat1d_custom6',
        '':'feat1d_custom7',
        '':'feat1d_custom8',
        '':'feat1d_custom9',
        '':'feat1d_custom10'  
    }

    dict_extra_feat1d_time = {
        '':'timestamp',
        '':'feat1d_time1',
        '':'feat1d_time2',
        '':'feat1d_time3',
        '':'feat1d_time4',
        '':'feat1d_time5',
        '':'feat1d_time6',
        '':'feat1d_time7',
        '':'feat1d_time8',
        '':'feat1d_time9',
        '':'feat1d_time10'  
    }

    dict_extra_feat3d = {
        '':'cod_sku',
        '':'cod_cliente',
        '':'timestamp',
        '':'feat3d_1',
        '':'feat3d_2',
        '':'feat3d_3',
        '':'feat3d_4',
        '':'feat3d_5',
        '':'feat3d_6',
        '':'feat3d_7',
        '':'feat3d_8',
        '':'feat3d_9',
        '':'feat3d_10'  
    }
    return dict_lowest_level, dict_meta_prod, dict_meta_custom, dict_meta_calend, dict_fato, dict_extra_feat1d_sku, dict_extra_feat1d_custom, dict_extra_feat1d_time, dict_extra_feat3d


# -----------------------------------------------------------------------------
def run_set_var_params():
    var_what = ['cod_sku']
    var_where = ['cod_cliente']
    var_when = ['timestamp']
    var_features = []
    var_target = ['yhat']
    
    return var_what, var_where, var_when, var_features, var_target


# -----------------------------------------------------------------------------
def run_scope_definition(df_input):
    dict_lowest_level,dict_meta_prod,dict_meta_custom,dict_meta_calend,dict_fato,dict_extra_feat1d_sku,dict_extra_feat1d_custom,dict_extra_feat1d_time,dict_extra_feat3d = run_setup_dict()
    df_input = df_input[['cod_sku','cod_cliente','timestamp','yhat','past_fcst1' , 'month','year']]
    
    return df_input