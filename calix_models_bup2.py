import copy
import pickle
import sys

import lightgbm as lgb
import numpy as np
import pandas as pd
import re

from catboost import Pool, CatBoostRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression 
from sklearn.model_selection import train_test_split

import optuna
optuna.logging.set_verbosity(optuna.logging.WARNING)

# -----------------------------------------------------------------------------
# CALIX MODULES
# -----------------------------------------------------------------------------
from calix_preprocess2 import *
from calix_model_library import *
from user_config import USER_PATH

# -----------------------------------------------------------------------------

class ObjectiveLGBM():

    def set_train_datasets(self, train_ds, val_size=0.2):
        self.train_ds = train_ds
        #self.train_ds.set_index(['cod_sku', 'cod_cliente'], inplace=True)
        #self.train_ds.drop(columns=['timestamp'], inplace=True)
        cat_features = self.train_ds.select_dtypes('object').columns.to_list()
        for c in cat_features:
            self.train_ds[c] = self.train_ds[c].astype('category')

        timestamp_list = self.train_ds['timestamp'].unique()
        train_val_delimiter = timestamp_list[int(len(timestamp_list)*(1-val_size))]

        self.val_ds = train_ds[train_ds['timestamp']>=train_val_delimiter]
        self.train_ds = train_ds[train_ds['timestamp']<train_val_delimiter]

        self.X_train = self.train_ds.drop(columns=['timestamp', 'yhat'])
        self.X_val = self.val_ds.drop(columns=['timestamp', 'yhat'])
        self.y_train = self.train_ds['yhat']
        self.y_val = self.val_ds['yhat']

        self.train_dataset = lgb.Dataset(self.X_train,label=self.y_train,
                                         categorical_feature=cat_features, 
                                         free_raw_data=False)

        self.val_dataset = lgb.Dataset(self.X_val,label=self.y_val,
                                         categorical_feature=cat_features, 
                                         free_raw_data=False)
    
    def __init__(self):
        self.best_booster = None
        self._booster = None

    def wmape(self, y_pred, y_true):
        y_true = y_true.get_label()
        return 'wmape', 100*sum(abs(y_pred-y_true))/sum(abs(y_true)), False
    def wmape2(self, y_pred, y_true):
        return 'wmape', 100*sum(abs(y_pred-y_true))/sum(abs(y_true)), False

    def __call__(self, trial):

        param = {
        'num_boost_round':trial.suggest_int('num_boost_round',100,300),
        'learning_rates':trial.suggest_float('learning_rates',0.01,1),
        'verbosity': -1,
        'boosting': trial.suggest_categorical('boosting',['gbdt','rf','dart']),
        'lambda_l1': trial.suggest_loguniform('lambda_l1', 1e-8, 10.0),
        'lambda_l2': trial.suggest_loguniform('lambda_l2', 1e-8, 10.0),
        'num_leaves': trial.suggest_int('num_leaves', 2, 256),
        'feature_fraction': trial.suggest_uniform('feature_fraction', 0.4, 1.0),
        'bagging_fraction': trial.suggest_uniform('bagging_fraction', 0.4, 1.0),
        'bagging_freq': trial.suggest_int('bagging_freq', 1, 7),
        'min_child_samples': trial.suggest_int('min_child_samples', 5, 100),
        #'tree_learner':'data'
    }
        
        # Add a callback for pruning.
        pruning_callback = optuna.integration.LightGBMPruningCallback(trial, 'wmape')
        gbm = lgb.train(params=param, train_set=self.train_dataset, 
                        valid_sets=[self.val_dataset], verbose_eval=False, 
                        callbacks=[pruning_callback], feval=self.wmape)
        
        self._booster = gbm
        
        preds = gbm.predict(self.X_val)
        _, wmape, _ = self.wmape2(preds, self.y_val)
        return wmape

    def callback(self, study, trial):
        if study.best_trial == trial:
            self.best_booster = self._booster

#------------------------------------------------------------------------------
def call_model_bup(x, cliente_isystems, model_bup, xtr, ytr, xts, yts, var_pack):

# Calculando a previsão no menor nível (BOTTOM UP)
    print("\nPrevisão BOTTOM UP iniciada...")
    if (model_bup == 'catboost'):
        p, mdl, bup_params, model_name = run_catboost2(x,cliente_isystems, xtr, ytr, xts, yts)      
        bup_params = bup_params
    elif (model_bup == 'lightgbm'):
        p, mdl, bup_params, model_name = run_lgbm2(x,cliente_isystems, xtr, ytr, xts, yts)      
        bup_params = bup_params
    elif (model_bup == 'lumos_bup'):

        # Abre o pack de variáveis necessário para rodar o modelo
        original, data_to_training, validation_data_per_aggregate, \
        validation_keys_per_aggregate = var_pack

        # Chama a função que faz o setup de todas as datas necessárias
        last_valid_month, user_start_validation, user_end_validation, \
            user_end_training, USER_VALIDATION_YEAR = data_setup()

        fcst_bup, mape_bup = run_lumos_bup(
            x, cliente_isystems, data_to_training, original, 
            validation_data_per_aggregate, validation_keys_per_aggregate, 
            user_start_validation, user_end_validation, last_valid_month)
    else:
        print('Modelo inexistente!')       
    print('Previsão bottom up concluída!')

    try:
        return fcst_bup, mape_bup
    except:
        return p, mdl, bup_params, model_name


# -----------------------------------------------------------------------------
def run_catboost2(x,cliente_isystems, xtr, ytr, xts, yts):

    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")

    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    dict_setup_fcst_flow,dict_setup_clst_params = run_setup_fcst_flow()
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()   

    if (dict_setup_fcst_flow['cluster_pipelines']==True):
        try:
            change_params = dict_hyper_catboost[x]
            dict_hyper_catboost[x] = copy.deepcopy(dict_hyper_catboost['default'])
            for key in change_params.keys():
                dict_hyper_catboost[x][key] = change_params[key]
            dict_hyper_catboost = dict_hyper_catboost[x]
        except:
            dict_hyper_catboost = dict_hyper_catboost['default']
    else:
        dict_hyper_catboost = dict_hyper_catboost['default']

    print(dict_hyper_catboost)

    # Definir index como cod_sku & cod_cliente
    xtr.set_index(['cod_sku','cod_cliente'],inplace=True)
    xts.set_index(['cod_sku','cod_cliente'],inplace=True)
    xtr.drop(columns='timestamp',inplace=True)
    xts.drop(columns='timestamp',inplace=True)

    #**** REVER ESTA REGRA
    #xtr.drop(columns=['clst_ABC_Class','clst_XYZ_Class','clst_lifecycle'],inplace=True)
    #xts.drop(columns=['clst_ABC_Class','clst_XYZ_Class','clst_lifecycle'],inplace=True)

    train_pool = Pool(xtr, ytr,cat_features=dict_hyper_catboost['cat_features'])
    test_pool = Pool(xts,yts,cat_features=dict_hyper_catboost['cat_features'])
    
    #cfi = xtr.select_dtypes(include='object').columns.to_list()

    #xtr,xts = run_ohe(xtr,xts,cfi)
    # Declaração e parametrização do modelo
    mdl = CatBoostRegressor(**dict_hyper_catboost, allow_writing_files=False)
    
    mdl.fit(train_pool, eval_set=(xts,yts), silent=True, plot=False)
    print("Treino Catboost concluído!")

    model_params = mdl.get_all_params()
    model_name = 'catboost'
    
    #Previsão do modelo
    p = mdl.predict(xts)
    print("Previsão BOTTOM UP via CatBoost concluída!")
    
    pkl_full_path = path_outputs+'\Pickle_BUP_Catboost_Model.pkl'  

    with open(pkl_full_path, 'wb') as file:
        pickle.dump(mdl, file)
    
    return p,mdl,model_params,model_name

#-----------------------------LIGHT GBM--------------------------------------------------------------------
def run_lgbm2(x,cliente_isystems, xtr, ytr, xts, yts):
    
    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")

    from calix_config import run_system_paths,run_setup_fcst_flow,run_setup_hyper,run_setup_dict,run_set_var_params,run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw,path_curated,path_outputs = run_system_paths()
    var_what,var_where,var_when,var_features,var_target = run_set_var_params()
    dict_setup_fcst_flow,dict_setup_clst_params = run_setup_fcst_flow()
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper()   

    if (dict_setup_fcst_flow['cluster_pipelines']==True):
        try:
            change_params = dict_hyper_lgbm[x]
            dict_hyper_lgbm[x] = copy.deepcopy(dict_hyper_lgbm['default'])
            for key in change_params.keys():
                dict_hyper_lgbm[x][key] = change_params[key]
            dict_hyper_lgbm = dict_hyper_lgbm[x]
        except:
            dict_hyper_lgbm = dict_hyper_lgbm['default']
    else:
        dict_hyper_lgbm = dict_hyper_lgbm['default']

    print(dict_hyper_lgbm)
    
    # Definir index como cod_sku & cod_cliente
    xtr.set_index(['cod_sku','cod_cliente'],inplace=True)
    xts.set_index(['cod_sku','cod_cliente'],inplace=True)
    # Df para train-test split no optuna
    xtr_full = xtr.copy()
    xtr.drop(columns='timestamp',inplace=True)
    xts.drop(columns='timestamp',inplace=True)

    # Listar vars categoricas
    cat_features = xtr.select_dtypes('object').columns.to_list()
    # Conversao p/ category (req do LGBM)
    for c in cat_features:
        xtr[c] = xtr[c].astype('category')
        xts[c] = xts[c].astype('category')

    try:
        optuna_optimization = dict_setup_fcst_flow['optuna_optimization']
    except:
        optuna_optimization = False

    if optuna_optimization:
        print('Otmização de hiperparâmetros pelo optuna ativa!')
        train_ds = xtr_full.copy() 
        train_ds['yhat'] = ytr.ravel()
        objective = ObjectiveLGBM()
        objective.set_train_datasets(train_ds)
        lgmbstudy = optuna.create_study(pruner=optuna.pruners.MedianPruner(n_warmup_steps=10), 
                                        direction="minimize")
        lgmbstudy.optimize(objective, n_trials=200, callbacks=[objective.callback])
        lgmb_best_model = objective.best_booster
        print('Parâmetros LGBM:')
        print(lgmb_best_model.params)
        p = lgmb_best_model.predict(xts)
        model_params = "params"
        model_name = 'light_gbm'

        return p, lgmb_best_model, model_params, model_name

    else:
        # Formando objetos Dataset
        train_lgb = lgb.Dataset(xtr,label=ytr.ravel(),categorical_feature=cat_features, free_raw_data=False)
        test_lgb = lgb.Dataset(xts,label=yts.ravel(),categorical_feature=cat_features, free_raw_data=False)

        # params default
        #params = {}
        params = dict_hyper_lgbm
        mdl_lgb = lgb.train(params, train_lgb)
        print("Treino LightGBM concluído!")

        p = mdl_lgb.predict(xts)
        print("Previsão BOTTOM UP via LightGBM concluída!")

        model_params = "params"
        model_name = 'light_gbm'

        Pkl_Filename = "Pickle_BUP_LGBm_Model.pkl"  

        with open(Pkl_Filename, 'wb') as file:
            pickle.dump(mdl_lgb, file)
            
        return p,mdl_lgb,model_params,model_name

# -----------------------------------------------------------------------------
def run_lumos_bup(x,
    cliente_isystems, data_to_training, original, validation_data_per_aggregate, 
    validation_keys_per_aggregate, user_start_validation, user_end_validation, 
    last_valid_month):

    # Referenciando local onde fica armazenado o código CONFIG do cliente
    sys.path.append(USER_PATH + cliente_isystems + "\\SETUP\\")

    from calix_config import run_system_paths, run_setup_fcst_flow, run_setup_hyper, \
        run_setup_dict, run_set_var_params, run_scope_definition

    # Atribuindo as variáveis provenientes do arquivo de configuração
    path_raw, path_curated, path_outputs = run_system_paths()
    var_what, var_where, var_when, var_features, var_target = run_set_var_params()
    dict_setup_fcst_flow = run_setup_fcst_flow()
    dict_hyper_lumos, dict_hyper_catboost, dict_hyper_lgbm = run_setup_hyper() 

    # Definindo variáveis necessárias
    dict_hyper_lumos = dict_hyper_lumos[x]
    DIFF_WEIGHTED_AVERAGE = dict_hyper_lumos['diff_weighted_average']
    DPG_SKU_PARAMS = dict_hyper_lumos['dpg_sku_params']
    USER_VALIDATION_YEAR = dict_hyper_lumos['user_validation_year']
    USER_CURRENT_MONTH = dict_hyper_lumos['user_curr_month']
    OPER_WEIGHT = dict_hyper_lumos['oper_weight']

    # Cria o meanmod e o adjumod para dpg_sku
    dpg_sku_model = create_model(
        'cod_lowest_level', data_to_training, validation_data_per_aggregate,
            validation_keys_per_aggregate, user_start_validation, 
            user_end_validation, DIFF_WEIGHTED_AVERAGE)

    # Parte-se direto do adjumod, não há votação entre modelos distintos
    dpg_sku_model.copy_prevision('adjumod', 'dynamod')
    dpg_sku_model.copy_prevision('adjumod', 'secomod')
    dpg_sku_mape = setup_dynamic_model(
        dpg_sku_model, USER_VALIDATION_YEAR, USER_CURRENT_MONTH, OPER_WEIGHT, 
        DPG_SKU_PARAMS)

    # Retorna as previsões para a base original
    new_original = original.set_index(['cod_lowest_level','year','month'])
    dpg_sku_data = join_with_original(new_original, dpg_sku_model.data_val)

    # Calcula o MAPE para os meses de validação (2020 completo e meses de 2021 com dados)
    # Equivale ao m1_mapes, m2_mapes, m3_mapes para o ajuste dos agregados
    mape_mes_calc = mape_mes(dpg_sku_data, dpg_sku_model, USER_VALIDATION_YEAR, USER_CURRENT_MONTH)

    first_prevision = (USER_VALIDATION_YEAR-1)*100+1
    total_months = 24

    ano_mes = first_prevision

    dpg_sku_mape = copy.deepcopy(mape_mes_calc)

    for month_index in range(total_months):

        year = ano_mes // 100
        month = ano_mes % 100
        ano_mes_1 = (ano_mes+1) if (month < 12) else (year+1)*100+(month+1)%12
        ano_mes_2 = (ano_mes+2) if (month < 11) else (year+1)*100+(month+2)%12

        if (ano_mes < USER_CURRENT_MONTH):

            dpg_sku_mape[ano_mes_2] = mape_mes_calc[ano_mes]

        else:

            dpg_sku_mape[ano_mes_2] = mape_mes_calc[last_valid_month]

        ano_mes = (ano_mes+1) if (month < 12) else (year+1)*100+1
    
    return dpg_sku_data, dpg_sku_mape

